export default {
  namespaced: true,
  state: {
    isSidebarToggled: false,
  },
  mutations: {
    TOGGLE_SIDEBAR: (state: any) => state.isSidebarToggled = !state.isSidebarToggled,
  },
};
