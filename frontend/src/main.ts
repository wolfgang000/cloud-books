import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'toastr/build/toastr.css';
import 'jquery';
import 'bootstrap';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEye,
  faCalendarAlt,
  faCheck,
  faBookOpen,
  faUser,
  faEllipsisV,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
  faCog,
  faKey,
  faTrash,
  faLocationArrow,
  faTicketAlt,
  faSyncAlt,
  faCloud,
  faArrowLeft,
  faQuestion,
} from '@fortawesome/free-solid-svg-icons';

import {
  faGoogleDrive,
  faDropbox
} from '@fortawesome/free-brands-svg-icons';

library.add(
  faEye,
  faCalendarAlt,
  faCheck,
  faBookOpen,
  faUser,
  faEllipsisV,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
  faCog,
  faKey,
  faTrash,
  faLocationArrow,
  faTicketAlt,
  faSyncAlt,
  faGoogleDrive,
  faDropbox,
  faCloud,
  faArrowLeft,
  faQuestion
);

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueLazyload from 'vue-lazyload';

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueLazyload);

Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 1
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
