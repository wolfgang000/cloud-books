import axios from 'axios';
import toastr from 'toastr';
import { store } from './store';
import router, { ROUTE_LOGIN } from '@/router';
import { Socket } from 'phoenix';
import { Book } from './models';

const CURRENT_USER_KEY = 'current_user';
export const cleanUserStateAndRedirectToLogin = (context: any) => {
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  accounts.deleteUser();
  context.commit('accounts/SET_CURRENT_USER', null, { root: true });
  router.push({ name: ROUTE_LOGIN });
};

const errorHandler = (error: any) => {
  if (error.response && (error.response.status === 500 || error.response.status > 500)) {
    toastr.error('There was a problem retrieving the data from the server.');
  } else if (error.response && error.response.status === 401 && router.currentRoute.name !== ROUTE_LOGIN) {
    toastr.warning('it seems your session has expired');
    cleanUserStateAndRedirectToLogin(store);
  }
  throw error;
};

export const accounts = {
  getUser() {
    const userStr = localStorage.getItem(CURRENT_USER_KEY);
    return userStr ? JSON.parse(userStr) : null;
  },
  setUser(user: string) {
    localStorage.setItem(CURRENT_USER_KEY, JSON.stringify(user));
  },
  deleteUser() {
    localStorage.removeItem(CURRENT_USER_KEY);
  },
  isAuthenticated() {
    if (this.getUser()) {
      return true;
    }
    return false;
  },
  login(email: string, password: string) {
    return axios
      .post(`/api/accounts/login`, { user: { email, password } })
      .then((r) => r.data)
      .catch(errorHandler);
  },
  signup(payload: { email: string; password: string }) {
    return axios
      .post(`/api/accounts/signup`, payload)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  logout() {
    return axios.post(`/api/accounts/logout`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getUserToken() {
    return axios.get(`/api/accounts/web_socket_token`)
      .then((r) => r.data.data)
      .catch(errorHandler);
  },
};

export const general = {
  getBooks(params) {
    return axios
      .get(`/api/books`, { params })
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getBook(bookId: number) {
    return axios
      .get(`/api/books/${bookId}`)
      .then((r) => r.data.data as Book)
      .catch(errorHandler);
  },
  updateBook(bookId: number, payload) {
    return axios
      .put(`/api/books/${bookId}`, payload)
      .then((r) => r.data.data as Book)
      .catch(errorHandler);
  },
  getStorageBuckets() {
    return axios
      .get(`/api/storage`)
      .then((r) => r.data.data as any[])
      .catch(errorHandler);
  },
  syncStorageBucket(storagId: number) {
    return axios
      .post(`/api/storage/${storagId}/sync`)
      .then((r) => r.data as any)
      .catch(errorHandler);
  },
  deleteStorageBucket(storagId: number) {
    return axios
      .delete(`/api/storage/${storagId}/`)
      .then((r) => r.data as any)
      .catch(errorHandler);
  },
};

const SOCKET_URL = '/api/socket';
let socket = new Socket(SOCKET_URL);
let userChannel = socket.channel('user');
export const WebSocket = {
  getSocket() {
    return socket;
  },
  setToken(token: string) {
    socket = new Socket(SOCKET_URL, {
      params: { token: token },
    });
  },
  setUserChannel(userId: number) {
    userChannel = socket.channel(`user:${userId}`);
    return userChannel;
  },
  getUserChannel() {
    return userChannel;
  },
  disconnect() {
    socket.disconnect();
  }
};
