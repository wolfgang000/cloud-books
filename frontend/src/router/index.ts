/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import HomeBaseView from '@/views/home/HomeBaseView.vue';
import HomeView from '@/views/home/HomeView.vue';
import HomeReadingView from '@/views/home/HomeReadingView.vue';
import HomeCompletedView from '@/views/home/HomeCompletedView.vue';
import HomePlanToReadView from '@/views/home/HomePlanToReadView.vue';
import HomeStorageView from '@/views/home/HomeStorageView.vue';
import Signup from '../views/Signup.vue';
import Login from '../views/Login.vue';
import { accounts } from '@/api';

export const ROUTE_LOGIN = 'login';
export const ROUTE_SIGNUP = 'signup';
export const ROUTE_BOOKS_HOME = 'books.home';
export const ROUTE_BOOKS_READING = 'books.reading';
export const ROUTE_BOOKS_COMPLETED = 'books.completed';
export const ROUTE_BOOKS_PLAN_TO_READ = 'books.plan_to_read';
export const ROUTE_STORAGE = 'storage';
export const ROUTE_READER_V2 = 'reader_v2';
export const ROUTE_TERMS_AND_CONDITIONS = 'terms-and-conditions';
export const ROUTE_PRIVACY_POLICY = 'privacy-policy'

function requireGuest(to: any, from: any, next: any) {
  next(!accounts.isAuthenticated() ? true : {
    name: ROUTE_BOOKS_HOME
  });
}

function requireUser(to: any, from: any, next: any) {
  // will login and come back
  next(accounts.isAuthenticated() ? true : {
    name: ROUTE_LOGIN,
    query: {
      redirect: to.fullPath,
    },
  });
}


Vue.use(VueRouter)
// TODO: add 404 page
const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: { name: ROUTE_BOOKS_HOME },
    beforeEnter: requireUser,
  },
  {
    path: '/home',
    component: Home,
    beforeEnter: requireUser,
    children: [
      {
        path: 'books',
        component: HomeBaseView,
        children: [
          {
            path: 'home',
            name: ROUTE_BOOKS_HOME,
            component: HomeView
          },
          {
            path: 'reading',
            name: ROUTE_BOOKS_READING,
            component: HomeReadingView
          },
          {
            path: 'completed',
            name: ROUTE_BOOKS_COMPLETED,
            component: HomeCompletedView
          },
          {
            path: 'plan_to_read',
            name: ROUTE_BOOKS_PLAN_TO_READ,
            component: HomePlanToReadView
          },
          {
            path: 'storage',
            name: ROUTE_STORAGE,
            component: HomeStorageView
          }
        ]
      },
      {
        path: 'reader/:bookId',
        name: ROUTE_READER_V2,
        component: () => import(/* webpackChunkName: "pdf_viewer" */ '../views/ReaderV2.vue'),
      },
    ]
  },
  {
    path: '/account/signup',
    name: ROUTE_SIGNUP,
    component: Signup,
    beforeEnter: requireGuest,
  },
  {
    path: '/account/login',
    name: ROUTE_LOGIN,
    component: Login,
    beforeEnter: requireGuest,
  },
  {
    path: '/terms-and-conditions',
    name: ROUTE_TERMS_AND_CONDITIONS,
    component: () => import(/* webpackChunkName: "terms-and-conditions" */ '../views/TermsAndConditions.vue'),
  },
  {
    path: '/privacy-policy',
    name: ROUTE_PRIVACY_POLICY,
    component: () => import(/* webpackChunkName: "privacy-policy" */ '../views/PrivacyPolicy.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
