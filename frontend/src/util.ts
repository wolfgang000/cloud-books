export const Util = {
  isNullOrWhiteSpace(s: any): boolean {
    return !(Boolean(s) && Boolean(s.trim()));
  },
  updateListItem(list: any[], item: any): any[] {
    return list.map((i) => i.id === item.id ? item : i)
  }
};