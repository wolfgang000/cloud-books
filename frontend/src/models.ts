/* eslint-disable @typescript-eslint/camelcase */
export enum StorageProvider {
  GoogleDrive = 1,
  Dropbox = 2
}

export enum ReadingStatus {
  Unknown = 1,
  Reading = 2,
  PlanToRead = 3,
  Completed = 4,
}

export function getStorageProviderStr(storageProvider: StorageProvider): string {
  switch (storageProvider) {
    case StorageProvider.GoogleDrive:
      return "Google Drive"
    default:
      return "";
  }
}

export interface UserStorageProvider {
  id: number;
  storage_provider_id: StorageProvider;
  is_synchronizing: boolean;
}

export interface Book {
  id: number;
  title: string;
  author: string;
  thumbnail_url: string;
  num_pages: number;
  current_page: number;
  reading_status_id: ReadingStatus;
}

export function GenNullBook(): Book {
  return {
    id: -1,
    title: '',
    author: '',
    num_pages: 0,
    current_page: 0,
    thumbnail_url: '',
    reading_status_id: ReadingStatus.Unknown,
  };
}