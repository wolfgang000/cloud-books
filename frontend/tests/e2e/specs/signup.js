module.exports = {
  beforeEach: async (browser) => {
    // TODO: figure out how to import functions to the test modules
    const axios = require('axios');
    await axios.delete(`${browser.options.launch_url}/api/testing/reset_data/`);
    return browser.init()
  },

  'Signup test': (browser) => {
    browser.page.signup().pause(1000).navigate().performSignup();
    browser.end();
  },
}
  