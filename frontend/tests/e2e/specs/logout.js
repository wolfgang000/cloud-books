module.exports = {
  beforeEach: async (browser) => {
    const axios = require('axios');
    await axios.delete(`${browser.options.launch_url}/api/testing/reset_data/`);
    await axios.post(`${browser.options.launch_url}/api/accounts/signup/`, { user: { name: "test",  email: "test@mail.com", password: "password" }});
    return browser.init()
  },

  'Login tests': (browser) => {
    browser.page.login().navigate().performLogin();
    browser.page.home().navigate().performLogout();
    browser.end();
  },
}
    