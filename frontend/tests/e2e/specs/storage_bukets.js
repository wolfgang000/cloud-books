/* eslint-disable @typescript-eslint/camelcase */
module.exports = {
  beforeEach: async (browser) => {
    const axios = require('axios');
    await axios.delete(`${browser.options.launch_url}/api/testing/reset_data/`)
    const signupResponse = await axios.post(`${browser.options.launch_url}/api/accounts/signup/`, { user: { name: "test",  email: "test@mail.com", password: "password" }});
    await axios.post(`${browser.options.launch_url}/api/testing/user_storage_provider`, { user_storage_provider: { 
        user_id: signupResponse.data.user.id, 
        account_name: "test storage",
        refresh_token: process.env.TEST_REFRESH_TOKEN, 
        account_id: process.env.TEST_ACCOUNT_ID,
        storage_provider_id: 1
      }
    })
    return browser.init()
  },

  'show google oauth page tests': (browser) => {
    browser.page.login().navigate().performLogin();
    browser.page.storage().navigate().goToGoogleOAuthPage();
    browser.end();
  },

  'show dropbox oauth page tests': (browser) => {
    browser.page.login().navigate().performLogin();
    browser.page.storage().navigate().goToDropboxOAuthPage();
    browser.end();
  },

  'sync tests': (browser) => {
    browser.page.login().navigate().performLogin();
    browser.page.storage().navigate().performSync();
    browser.page.home().navigate().waitForElementVisible('div[id="book-card-list"] div:nth-child(1)')
    browser.end();
  },
}
      