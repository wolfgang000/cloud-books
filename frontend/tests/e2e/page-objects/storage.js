const commands = {
    performLogout() {
      return this
        .waitForElementVisible('@dropdownMenuButton', 1000)
        .click('@dropdownMenuButton')
        .assert.visible('@logoutButton')
        .click('@logoutButton')
        .waitForElementNotPresent('@dropdownMenuButton', 1000);
    },
    goToGoogleOAuthPage() {
      return this
        .waitForElementVisible('@addGoogleDriveBucketStorageLink', 1000)
        .click('@addGoogleDriveBucketStorageLink')
        .assert.urlContains('accounts.google.com')
        .waitForElementVisible('input[type=email]', 1000)
        .assert.visible('input[type=email]')
    },
    goToDropboxOAuthPage() {
      return this
        .waitForElementVisible('@addDropboxBucketStorageLink', 1000)
        .click('@addDropboxBucketStorageLink')
        .assert.urlContains('dropbox.com/oauth2/authorize')
        .waitForElementVisible('input[type=email]', 1000)
        .assert.visible('input[type=email]')
    },
    performSync() {
      return this
        .pause(1000)
        .click('@firstSyncButton')
        .waitForElementVisible('@toastrNotification', 40000)
        .assert.containsText('@toastrNotification', 'Sync complete')
    },
  };
    
  module.exports = {
    url() {
      return `/home/books/storage`;
    },
    commands: [commands],
    elements: {
      dropdownMenuButton: 'button[id="dropdownMenuButton"]',
      logoutButton: 'button[id="logout_button"]',
      addGoogleDriveBucketStorageLink: 'a[id="google_drive_link"]',
      addDropboxBucketStorageLink: 'a[id="dropbox_link"]',
      firstSyncButton: {
        selector: '//*[@id="storage_table"]/tbody/tr/td[2]/div/button[1]',
        locateStrategy: 'xpath'
      },
      toastrNotification: {
        selector: '//*[@id="toast-container"]',
        locateStrategy: 'xpath'
      }
    },
  };
  