const commands = {
  performLogout() {
    return this
      .waitForElementVisible('@dropdownMenuButton', 1000)
      .click('@dropdownMenuButton')
      .assert.visible('@logoutButton')
      .click('@logoutButton')
      .waitForElementNotPresent('@dropdownMenuButton', 1000);
  },
};
  
module.exports = {
  url() {
    return `/home/books/home`;
  },
  commands: [commands],
  elements: {
    dropdownMenuButton: 'button[id="dropdownMenuButton"]',
    logoutButton: 'button[id="logout_button"]',
  },
};
