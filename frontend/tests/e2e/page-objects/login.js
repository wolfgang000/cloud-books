const loginCommands = {
  performLogin(email = 'test@mail.com', password = 'password') {
    this.api.pause(1000);
    return this
      .waitForElementVisible('@submitButton', 1000)
      .assert.visible('@emailField')
      .assert.visible('@passwordField')
      .setValue('@emailField', email)
      .setValue('@passwordField', password)
      .click('@submitButton')
      .waitForElementNotPresent('@submitButton', 1000);
  },
};
  
module.exports = {
  url() {
    return `/account/login`;
  },
  commands: [loginCommands],
  elements: {
    emailField: 'input[type=email]',
    passwordField: 'input[type=password]',
    submitButton: 'button[id="login_button"]',
  },
};
  
  