/**
 * A Nightwatch page object. The page object name is the filename.
 *
 * Example usage:
 *   browser.page.homepage.navigate()
 *
 * For more information on working with page objects see:
 *   https://nightwatchjs.org/guide/working-with-page-objects/
 *
 */

const commands = {
  performSignup(fullName = 'fake user', email = 'test@mail.com', password = 'password123') {
    return this
      .waitForElementVisible('@submitButton', 1000)
      .assert.visible('@fullNameField')
      .assert.visible('@emailField')
      .assert.visible('@passwordField')
      .setValue('@fullNameField', fullName)
      .setValue('@emailField', email)
      .setValue('@passwordField', password)
      .click('@submitButton')
      .waitForElementNotPresent('@submitButton', 1000);
  },
};


module.exports = {
  url() {
    return `/account/signup`;
  },
  commands: [commands],
  elements: {
    fullNameField: 'input[id="full_name"]',
    emailField: 'input[id="email"]',
    passwordField: 'input[id="password"]',
    submitButton: 'button[id="signup_button"]',
  },
}
