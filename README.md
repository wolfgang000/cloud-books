# Docker compose commands

App running at: http://localhost:8000

## Build local
```
touch backend/.env.local.docker
touch frontend/.env.local.docker
docker-compose build --build-arg USER_ID=$(id -u)   --build-arg GROUP_ID=$(id -g)
docker-compose up
```
In order to access to the google drive api, we need to generate the google oauth credentials
 - Follow [this](https://developers.google.com/identity/protocols/oauth2/web-server#creatingcred) google's tutorial to generate the authorization credentials
 - Add a new redirect URI "http://localhost:8000/api/accounts/oauth/google_callback"
 - Enable the google drive api [link here](https://developers.google.com/drive/api/v3/enable-drive-api)
 - Finally set the redirect_uri, google_secret and google_client_id as environment variables in the backend/.env.local.docker file with the following names DEV_GOOGLE_REDIRECT_URI, DEV_GOOGLE_SECRET, DEV_GOOGLE_CLIENT_ID


## Run local migrations
```
docker-compose exec backend mix ecto.migrate
```

## Run backend tests
```
docker-compose exec backend mix test
```



## Run the e2e tests inside docker-compose selenium container
WARNING: For the moment the e2e tests use the db_dev to run the tests meaning that all the data in the database is going to be deleted
TODO: switch to the db_test
```
docker-compose exec frontend yarn test:e2e --env docker-compose-chrome --url http://router
```

## Run the e2e tests on the host
```
# change the "start_process" value in the file frontend/nightwatch.json from false to true

cd frontend
yarn test:e2e --url http://localhost:8000

# Run a specific test
yarn test:e2e --url http://localhost:8000 -f signup.js
```

# Dokku Deployment

## Setup server
```
# Install dokku
# wget https://raw.githubusercontent.com/dokku/dokku/v0.23.0/bootstrap.sh
# DOKKU_TAG=v0.23.0 bash bootstrap.sh
# dokku plugin:install https://github.com/dokku/dokku-postgres.git
# dokku plugin:install https://gitlab.com/notpushkin/dokku-monorepo
# dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git

dokku apps:create cloud-books-back
dokku domains:disable cloud-books-back
dokku config:set cloud-books-back \
  # Set the variables from backend/.env.example

dokku apps:create cloud-books-front
dokku config:set cloud-books-front \
  # Set the variables from frontend/.env.example

# Setup local database
dokku postgres:create cloud-books-db
dokku postgres:link cloud-books-db cloud-books-back

# Setup SSL certificate
# Remember to open the 443 port
dokku domains:add cloud-books-front cloud-books.example.com
dokku config:set --no-restart cloud-books-front DOKKU_LETSENCRYPT_EMAIL=test@mail.com
dokku letsencrypt cloud-books-front
```

## Deploy and push changes
```
git remote add server-backend dokku@example.com:cloud-books-back
git remote add server-frontend dokku@example.com:cloud-books-front

git push server-backend
git push server-frontend
```

# Production debugging

## Enter to the container
```
dokku enter cloud-books-back web /bin/sh
```

## Open a remote elixir console
```
dokku enter cloud-books-back web prod/rel/cloud_books/bin/cloud_books remote
```

## Show logs
```
dokku logs cloud-books-back
```

## Open a psql terminal
```
dokku postgres:connect cloud-books-db
```

