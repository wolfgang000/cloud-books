defmodule CloudBooks.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:storage_providers) do
      add :name, :string, null: false
    end

    execute("""
      INSERT INTO storage_providers (id, name) VALUES
        (1, 'google drive'),
        (2, 'dropbox')
    ;
    """)

    create table(:users) do
      add :email, :string, null: false
      add :name, :string
      add :password_hash, :binary
      add :last_login, :utc_datetime
      timestamps(type: :utc_datetime)
    end

    create unique_index(:users, [:email])

    create table(:sessions) do
      add :session_key, :string, size: 44, null: false
      add :expiration_date, :utc_datetime, null: false
      add :inserted_at, :utc_datetime, null: false
      add :user_id, references(:users), null: false
    end

    create unique_index(:sessions, [:session_key])

    create table(:user_storage_provider) do
      add :user_id, references(:users), null: false
      add :storage_provider_id, references(:storage_providers), null: false
      add :refresh_token_encrypted, :binary, null: false
      add :account_id, :string, null: false
      add :account_name, :string, null: false
      add :is_synchronizing, :boolean, null: false, default: false
      add :last_started_at, :utc_datetime, null: true
      add :last_ended_at, :utc_datetime, null: true

      timestamps(type: :utc_datetime)
    end

    create unique_index(:user_storage_provider, [:account_id, :storage_provider_id], name: :user_storage_provider_account_id_storage_provider_id_index)

    create table(:reading_status) do
      add :name, :string, null: false
    end

    execute("""
      INSERT INTO reading_status (id, name) VALUES
        (1, 'unknown'),
        (2, 'reading'),
        (3, 'plan to read'),
        (4, 'completed')
    ;
    """)

    create table(:books) do
      add :title, :string, null: false
      add :author, :string
      add :thumbnail_s3_object_key, :string
      add :file_id, :string, null: false
      add :file_name, :string, null: false
      add :num_pages, :integer, null: false
      add :current_page, :integer, null: false

      add :reading_status_id, references(:reading_status), null: false, default: 1
      add :user_storage_provider_id, references(:user_storage_provider, on_delete: :delete_all),
        null: false

      timestamps(type: :utc_datetime)
    end

    create unique_index(:books, [:file_id, :user_storage_provider_id])
  end
end
