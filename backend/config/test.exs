use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :cloud_books, CloudBooks.Repo,
  username: System.get_env("TEST_DB_USER") || "test_user",
  password: System.get_env("TEST_DB_PASSWORD") || "test_password",
  database:
    System.get_env("TEST_DB_NAME") || "cloud_books_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("TEST_DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :cloud_books, CloudBooksWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: (System.get_env("LOGGER_LEVEL") || "warn") |> String.to_atom()

config :cloud_books,
  google_oauth: CloudBooksWeb.GoogleOAuthMock,
  dropbox_oauth: CloudBooksWeb.DropboxOAuthMock,
  storage_provider_api_helper: CloudBooks.Helper.StorageProviderApiHelperMock

config :cloud_books, CloudBooks.Encryption.AES,
  keys: [
    :base64.decode("vA/K/7K6Z3obnTxlPx6fDuy/tiPj4FS7dDtUpfvRbG4=")
  ]

config :argon2_elixir, t_cost: 1, m_cost: 8

config :cloud_books, CloudBooks.Mailer,
  adapter: Bamboo.TestAdapter,
  default_from_address: "test@mail.com"
