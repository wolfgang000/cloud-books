# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :cloud_books,
  ecto_repos: [CloudBooks.Repo]

# Configures the endpoint
config :cloud_books, CloudBooksWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "697VU0QJ00Im8jF3/Uuxq1KfZTnO/I9m+l1TFziPxHNmdSk+9jMl9AdMxScpEuuL",
  render_errors: [view: CloudBooksWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: CloudBooks.PubSub,
  live_view: [signing_salt: "OL2ylGqj"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :cloud_books,
  google_oauth: CloudBooksWeb.GoogleOAuth,
  dropbox_oauth: CloudBooksWeb.DropboxOAuth,
  storage_provider_api_helper: CloudBooks.Helper.StorageProviderApiHelper

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
