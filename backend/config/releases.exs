import Config

defmodule ReleaseUtil do
  def get_env_or_raise_error(var_name),
    do:
      System.get_env(var_name) ||
        raise("""
        environment variable #{var_name} is missing.
        """)
end

secret_key_base = ReleaseUtil.get_env_or_raise_error("SECRET_KEY_BASE")
mailgun_apy_key = ReleaseUtil.get_env_or_raise_error("MAILGUN_API_KEY")
mailgun_domain = ReleaseUtil.get_env_or_raise_error("MAILGUN_DOMAIN")
default_from_address = ReleaseUtil.get_env_or_raise_error("DEFAULT_FROM_ADDRESS")

server_host = ReleaseUtil.get_env_or_raise_error("SERVER_HOST")
db_url = ReleaseUtil.get_env_or_raise_error("DATABASE_URL")
db_pool_size = System.get_env("POOL_SIZE") || "10"
secret_key_base = ReleaseUtil.get_env_or_raise_error("SECRET_KEY_BASE")

google_client_id = System.get_env("GOOGLE_CLIENT_ID")
google_client_secret = System.get_env("GOOGLE_SECRET")
google_redirect_uri = System.get_env("GOOGLE_REDIRECT_URI")
dropbox_client_id = System.get_env("DROPBOX_CLIENT_ID")
dropbox_client_secret = System.get_env("DROPBOX_SECRET")
dropbox_redirect_uri = System.get_env("DROPBOX_REDIRECT_URI")

s3_access_key_id = ReleaseUtil.get_env_or_raise_error("S3_ACCESS_KEY_ID")
s3_secret_access_key = ReleaseUtil.get_env_or_raise_error("S3_SECRET_ACCESS_KEY")
s3_scheme = ReleaseUtil.get_env_or_raise_error("S3_SCHEME")
s3_host = ReleaseUtil.get_env_or_raise_error("S3_HOST")
s3_port = ReleaseUtil.get_env_or_raise_error("S3_PORT")
s3_bucket = ReleaseUtil.get_env_or_raise_error("S3_BUCKET")

encryption_keys =
  ReleaseUtil.get_env_or_raise_error("ENCRYPTION_KEYS")
  |> String.replace("'", "")
  |> String.split(",")
  |> Enum.map(fn key -> :base64.decode(key) end)

logger_level = (System.get_env("LOGGER_LEVEL") || "info") |> String.to_atom()

config :logger, level: logger_level

config :cloud_books, CloudBooks.Repo,
  url: db_url,
  pool_size: String.to_integer(db_pool_size)

config :phoenix, :filter_parameters, ["password", "secret", "session_key", "token"]

config :cloud_books, CloudBooksWeb.Endpoint,
  server: true,
  url: [host: server_host],
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base

config :oauth2,
  google_client_id: google_client_id,
  google_client_secret: google_client_secret,
  google_redirect_uri: google_redirect_uri,
  dropbox_client_id: dropbox_client_id,
  dropbox_client_secret: dropbox_client_secret,
  dropbox_redirect_uri: dropbox_redirect_uri

config :ex_aws,
  json_codec: Jason,
  access_key_id: s3_access_key_id,
  secret_access_key: s3_secret_access_key

config :ex_aws, :s3,
  scheme: s3_scheme,
  host: s3_host,
  port: String.to_integer(s3_port),
  bucket: s3_bucket

config :cloud_books, CloudBooks.Encryption.AES, keys: encryption_keys

config :cloud_books, CloudBooks.Mailer,
  adapter: Bamboo.MailgunAdapter,
  domain: mailgun_domain,
  api_key: mailgun_apy_key,
  hackney_opts: [
    recv_timeout: :timer.minutes(1)
  ],
  default_from_address: default_from_address
