defmodule CloudBooks.Worker.TokenStoreTest do
  use ExUnit.Case
  alias CloudBooks.Worker.TokenStore
  alias CloudBooks.StorageProviderToken

  test "should insert token" do
    expiration_date = DateTime.utc_now()

    assert {:ok,
            %StorageProviderToken{
              access_token: "test_token",
              expires_at: ^expiration_date,
              storage_provider_id: 1
            }} = TokenStore.insert_access_token(1, "test_token", expiration_date, 1)

    assert {:ok,
            %StorageProviderToken{
              access_token: "test_token",
              expires_at: ^expiration_date,
              storage_provider_id: 1
            }} = TokenStore.get_access_token(1)
  end

  test "should return not_found on nonexistent token" do
    assert {:error, :not_found} = TokenStore.get_access_token(999)
  end
end
