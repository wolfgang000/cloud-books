defmodule CloudBooks.Helper.TokenManagerTest do
  use CloudBooksWeb.ConnCase
  import CloudBooks.Factory
  import Mox
  alias CloudBooks.Helper.TokenManager
  alias CloudBooks.Worker.TokenStore
  alias CloudBooks.StorageProviderToken

  test "should get existing token" do
    expiration_date = DateTime.utc_now() |> DateTime.add(3600, :second)
    TokenStore.insert_access_token(1, "test_token", expiration_date, 1)

    assert {:ok,
            %StorageProviderToken{
              access_token: "test_token",
              expires_at: ^expiration_date,
              storage_provider_id: 1
            }} = TokenManager.get_access_token(1)
  end

  test "should update expired token" do
    expiration_date_new = DateTime.utc_now() |> DateTime.add(3600, :second)

    CloudBooksWeb.GoogleOAuthMock
    |> expect(:get_refresh_token, fn _params ->
      {:ok,
       %OAuth2.Client{
         token: %OAuth2.AccessToken{
           access_token: "test_token_new",
           expires_at: DateTime.to_unix(expiration_date_new),
           other_params: %{"scope" => "https://www.googleapis.com/auth/drive.readonly"},
           refresh_token: "test_refresh_token",
           token_type: "Bearer"
         }
       }}
    end)

    user_storage_provider = insert(:user_storage_provider)
    expiration_date_old = DateTime.utc_now() |> DateTime.add(-3600, :second)

    TokenStore.insert_access_token(
      user_storage_provider.id,
      "test_token_old",
      expiration_date_old,
      1
    )

    assert {:ok,
            %StorageProviderToken{
              access_token: "test_token_new",
              expires_at: token_expiration_date,
              storage_provider_id: 1
            }} = TokenManager.get_access_token(user_storage_provider.id)

    assert DateTime.diff(token_expiration_date, expiration_date_new, :second) < 1
  end

  test "should return error invalid user_storage_provider_id" do
    assert {:error, "The user_storage_provider_id: -1 seems to be invalid"} =
             TokenManager.get_access_token(-1)
  end
end
