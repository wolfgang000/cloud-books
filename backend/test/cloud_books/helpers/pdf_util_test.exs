defmodule CloudBooks.Helper.PDFUtilTest do
  use ExUnit.Case
  alias CloudBooks.Helper.PDFUtil

  test "should generate image from pdf" do
    input_pdf = "test/data/pdf_without_metadata.pdf"
    output_image = "/tmp/#{DateTime.to_string(DateTime.utc_now())}"

    assert :ok ==
             PDFUtil.pdf_to_cairo([
               "-png",
               "-q",
               "-singlefile",
               "-scale-to",
               "540",
               input_pdf,
               output_image
             ])

    assert File.exists?(output_image <> ".png")
  end

  test "should not generate image from corructed pdf" do
    input_pdf = "test/data/pdf_corructed.pdf"
    output_image = "/tmp/#{DateTime.to_string(DateTime.utc_now())}"

    assert {:error, _} =
             PDFUtil.pdf_to_cairo([
               "-png",
               "-q",
               "-singlefile",
               "-scale-to",
               "540",
               input_pdf,
               output_image
             ])

    refute File.exists?(output_image <> ".png")
  end

  test "should get metadata from pdf" do
    input_pdf = "test/data/pdf_with_metadata.pdf"
    assert {:ok, metadata} = PDFUtil.get_pdf_metadata(input_pdf)

    assert %{
             author: "Fake Author",
             is_encrypted: false,
             keywords: "some, random, keywords",
             num_pages: 1,
             subject: "Fake Subject",
             title: "Fake Title"
           } = metadata
  end

  test "should not get metadata from corructed pdf" do
    input_pdf = "test/data/pdf_corructed.pdf"
    assert {:error, _} = PDFUtil.get_pdf_metadata(input_pdf)
  end

  test "should not get at least some data from pdf without metadata" do
    input_pdf = "test/data/pdf_without_metadata.pdf"
    assert {:ok, metadata} = PDFUtil.get_pdf_metadata(input_pdf)
    assert %{is_encrypted: false, num_pages: 1} = metadata
  end
end
