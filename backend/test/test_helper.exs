ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(CloudBooks.Repo, :manual)
Mox.defmock(CloudBooksWeb.GoogleOAuthMock, for: CloudBooksWeb.OAuthBehaviour)
Mox.defmock(CloudBooksWeb.DropboxOAuthMock, for: CloudBooksWeb.OAuthBehaviour)

Mox.defmock(CloudBooks.Helper.StorageProviderApiHelperMock,
  for: CloudBooks.Helper.StorageProviderApiHelperBehaviour
)
