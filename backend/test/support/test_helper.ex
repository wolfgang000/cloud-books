defmodule CloudBooks.TestHelper do
  import CloudBooks.Factory
  import Phoenix.ChannelTest
  @endpoint CloudBooksWeb.Endpoint

  def gen_authenticated_conn(conn) do
    user = insert(:user)
    session = insert(:session, %{user: user})
    unauthorize_conn = conn

    conn =
      conn
      |> Plug.Test.init_test_session(%{})
      |> Plug.Conn.put_session(:current_session_key, session.session_key)

    {:ok, conn: conn, unauthorize_conn: unauthorize_conn, user: user, session: session}
  end

  def gen_authenticated_socket(%{user: user, session: session}) do
    socket =
      socket(CloudBooksWeb.UserSocket, "users_socket:#{user.id}", %{
        current_user_id: user.id,
        current_user: user,
        current_session_id: session.id
      })

    {:ok, socket: socket, user: user, session: session}
  end

  def gen_subscribed_socket(%{socket: socket, user: %{id: user_id}}) do
    {:ok, _, subscribed_socket} =
      socket
      |> subscribe_and_join(CloudBooksWeb.UserChannel, "user:#{user_id}")

    {:ok, %{subscribed_socket: subscribed_socket}}
  end

  def is_sorted_desc([_ | []]), do: true

  def is_sorted_desc([head | tail]) do
    [next_item | _] = tail

    if head >= next_item do
      is_sorted_desc(tail)
    else
      false
    end
  end
end
