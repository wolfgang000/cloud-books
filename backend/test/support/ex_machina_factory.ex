defmodule CloudBooks.Factory do
  use ExMachina.Ecto, repo: CloudBooks.Repo

  def user_factory do
    %CloudBooks.Accounts.User{
      email: sequence(:email, &"email-#{&1}@example.com")
    }
  end

  def storage_provider_factory do
    %CloudBooks.Accounts.StorageProvider{
      name: sequence("serial")
    }
  end

  def user_storage_provider_factory do
    %CloudBooks.Accounts.UserStorageProvider{
      user: build(:user),
      account_id: sequence("serial"),
      account_name: sequence("serial"),
      refresh_token_encrypted:
        sequence(:refresh_token_encrypted, &CloudBooks.Encryption.AES.encrypt/1),
      storage_provider_id: sequence(:role_id, [1]),
      is_synchronizing: false
    }
  end

  def session_factory do
    %CloudBooks.Accounts.Session{
      user: build(:user),
      session_key: sequence("session_key"),
      expiration_date: DateTime.add(DateTime.utc_now(), 60 * 15, :second),
      inserted_at: DateTime.utc_now()
    }
  end

  def book_factory do
    %CloudBooks.Core.Book{
      title: sequence("serial"),
      author: sequence("serial"),
      file_id: sequence("serial"),
      file_name: sequence("serial"),
      num_pages: 100,
      current_page: 1,
      user_storage_provider: build(:user_storage_provider),
      reading_status_id: sequence(:reading_status_id, [1])
    }
  end
end
