defmodule CloudBooksWeb.UserChannelTest do
  use CloudBooksWeb.ChannelCase
  import CloudBooks.Factory
  alias CloudBooks.TestHelper

  defp create_user_and_session(_) do
    user = insert(:user)
    session = insert(:session, %{user: user})
    {:ok, user: user, session: session}
  end

  defp gen_authenticated_socket(params) do
    TestHelper.gen_authenticated_socket(params)
  end

  defp gen_subscribed_socket(params) do
    TestHelper.gen_subscribed_socket(params)
  end

  describe "user_channel" do
    setup [:create_user_and_session, :gen_authenticated_socket, :gen_subscribed_socket]

    test "ping replies with status ok", %{subscribed_socket: socket} do
      ref = push(socket, "ping", %{"hello" => "there"})
      assert_reply ref, :ok, %{"hello" => "there"}
    end

    test "shout broadcasts to user:lobby", %{subscribed_socket: socket} do
      push(socket, "shout", %{"hello" => "all"})
      assert_broadcast "shout", %{"hello" => "all"}
    end

    test "broadcasts are pushed to the client", %{subscribed_socket: socket} do
      broadcast_from!(socket, "broadcast", %{"some" => "data"})
      assert_push "broadcast", %{"some" => "data"}
    end

    test "try to join channel with wrong user_id", %{socket: socket, user: user} do
      assert {:error, %{reason: "unauthorized"}} =
               socket
               |> subscribe_and_join(CloudBooksWeb.UserChannel, "user:#{user.id + 1}")
    end
  end
end
