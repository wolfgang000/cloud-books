defmodule CloudBooksWeb.BrowserChannelTest do
  use CloudBooksWeb.ChannelCase
  import CloudBooks.Factory
  alias CloudBooksWeb.Helper.WebSocketTokenHelper

  describe "authentication" do
    test "with valid token" do
      %{
        id: session_id,
        user: %{
          id: user_id
        }
      } = session = insert(:session)

      token = WebSocketTokenHelper.sign_data(@endpoint, session.id)
      assert {:ok, socket} = connect(CloudBooksWeb.UserSocket, %{"token" => token})
      assert socket.assigns.current_session_id == session_id
      assert socket.assigns.current_user_id == user_id
    end

    test "with invalid token" do
      assert :error = connect(CloudBooksWeb.UserSocket, %{"token" => "invalid-token"})
    end

    test "without token" do
      assert :error = connect(CloudBooksWeb.UserSocket, %{})
    end
  end
end
