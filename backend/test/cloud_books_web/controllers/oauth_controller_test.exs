defmodule CloudBooksWeb.OAuthControllerTest do
  use CloudBooksWeb.ConnCase
  import CloudBooks.Factory
  alias CloudBooks.{Accounts, TestHelper}
  import Mox

  alias CloudBooks.Accounts

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_conn(conn)
  end

  describe "google OAuth" do
    setup [:create_authenticated_conn]

    test "create user_storage_provider successfuly", %{conn: conn, user: user} do
      CloudBooksWeb.GoogleOAuthMock
      |> expect(:get_access_token, fn _params ->
        {:ok,
         %OAuth2.Client{
           token: %OAuth2.AccessToken{
             access_token: "test_access_token",
             expires_at: 1_605_309_882,
             other_params: %{"scope" => "https://www.googleapis.com/auth/drive.readonly"},
             refresh_token: "test_refresh_token",
             token_type: "Bearer"
           }
         }}
      end)

      CloudBooks.Helper.StorageProviderApiHelperMock
      |> expect(:get_account_info, fn _params ->
        {:ok, %{id: "test_id", name: "name"}}
      end)

      conn = get(conn, Routes.o_auth_path(conn, :google_callback), code: "test")
      assert redirected_to(conn, 302) =~ "/#account_successfully_added"
      assert [_] = Accounts.list_users_storage_providers(user.id)
    end

    test "try to create user_storage_provider with repeated account_id", %{conn: conn, user: user} do
      CloudBooksWeb.GoogleOAuthMock
      |> expect(:get_access_token, fn _params ->
        {:ok,
         %OAuth2.Client{
           token: %OAuth2.AccessToken{
             access_token: "test_access_token",
             expires_at: 1_605_309_882,
             other_params: %{"scope" => "test"},
             refresh_token: "test_refresh_token",
             token_type: "Bearer"
           }
         }}
      end)

      CloudBooks.Helper.StorageProviderApiHelperMock
      |> expect(:get_account_info, fn _params ->
        {:ok, %{id: "test_id", name: "name"}}
      end)

      insert(:user_storage_provider, user: user, account_id: "test_id", storage_provider_id: 1)

      conn = get(conn, Routes.o_auth_path(conn, :google_callback), code: "test")
      assert redirected_to(conn, 302) =~ "/#account_already_added"
      assert [_] = Accounts.list_users_storage_providers(user.id)
    end

    test "create authorize url", %{conn: conn} do
      Mox.stub_with(CloudBooksWeb.GoogleOAuthMock, CloudBooksWeb.GoogleOAuth)
      conn = get(conn, Routes.o_auth_path(conn, :google_authorize_url))
      assert authorize_url = redirected_to(conn, 302)
      assert String.starts_with?(authorize_url, "https://accounts.google.com")
      assert String.contains?(authorize_url, "access_type=offline")
      assert String.contains?(authorize_url, "drive.readonly")
    end
  end

  describe "dropbox OAuth" do
    setup [:create_authenticated_conn]

    test "create user_storage_provider successfuly", %{conn: conn, user: user} do
      CloudBooksWeb.DropboxOAuthMock
      |> expect(:get_access_token, fn _params ->
        {:ok,
         %OAuth2.Client{
           token: %OAuth2.AccessToken{
             access_token: "test_access_token",
             expires_at: 1_605_309_882,
             other_params: %{"scope" => "scopes"},
             refresh_token: "test_refresh_token",
             token_type: "Bearer"
           }
         }}
      end)

      CloudBooks.Helper.StorageProviderApiHelperMock
      |> expect(:get_account_info, fn _params ->
        {:ok, %{id: "test_id", name: "name"}}
      end)

      conn = get(conn, Routes.o_auth_path(conn, :dropbox_callback), code: "test")
      assert redirected_to(conn, 302) =~ "/#account_successfully_added"
      assert [_] = Accounts.list_users_storage_providers(user.id)
    end

    test "try to create user_storage_provider with repeated account_id", %{conn: conn, user: user} do
      CloudBooksWeb.DropboxOAuthMock
      |> expect(:get_access_token, fn _params ->
        {:ok,
         %OAuth2.Client{
           token: %OAuth2.AccessToken{
             access_token: "test_access_token",
             expires_at: nil,
             other_params: %{"scope" => "scopes"},
             refresh_token: "test_refresh_token",
             token_type: "Bearer"
           }
         }}
      end)

      CloudBooks.Helper.StorageProviderApiHelperMock
      |> expect(:get_account_info, fn _params ->
        {:ok, %{id: "test_id", name: "name"}}
      end)

      insert(:user_storage_provider, user: user, account_id: "test_id", storage_provider_id: 2)

      conn = get(conn, Routes.o_auth_path(conn, :dropbox_callback), code: "test")
      assert redirected_to(conn, 302) =~ "/#account_already_added"
      assert [_] = Accounts.list_users_storage_providers(user.id)
    end

    test "create authorize url", %{conn: conn} do
      Mox.stub_with(CloudBooksWeb.DropboxOAuthMock, CloudBooksWeb.DropboxOAuth)
      conn = get(conn, Routes.o_auth_path(conn, :dropbox_authorize_url))
      assert authorize_url = redirected_to(conn, 302)
      assert String.starts_with?(authorize_url, "https://www.dropbox.com/oauth2/")
      assert String.contains?(authorize_url, "token_access_type=offline")
      assert String.contains?(authorize_url, "files.content.read")
    end
  end
end
