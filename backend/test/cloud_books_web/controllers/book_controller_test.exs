defmodule CloudBooksWeb.BookControllerTest do
  use CloudBooksWeb.ConnCase
  import CloudBooks.Factory

  alias CloudBooks.TestHelper
  alias CloudBooks.Core.{Book, ReadingStatus}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_authenticated_conn]

    test "lists user's books", %{conn: conn, user: user} do
      user_storage_provider = insert(:user_storage_provider, user: user)
      insert_list(5, :book, user_storage_provider: user_storage_provider)
      insert_list(3, :book)

      conn = get(conn, Routes.book_path(conn, :index))
      assert users_books = json_response(conn, 200)["entries"]
      assert length(users_books) == 5
    end

    test "lists paginated books response", %{conn: conn, user: user} do
      user_storage_provider = insert(:user_storage_provider, user: user)
      insert_list(25, :book, user_storage_provider: user_storage_provider)

      insert_list(25, :book,
        user_storage_provider: user_storage_provider,
        updated_at: DateTime.add(DateTime.utc_now(), 5, :second)
      )

      conn = get(conn, Routes.book_path(conn, :index))
      assert first_page_books = json_response(conn, 200)["entries"]
      assert first_page_metadata = json_response(conn, 200)["metadata"]
      assert length(first_page_books) == first_page_metadata["limit"]

      conn =
        get(conn, Routes.book_path(conn, :index), next_page_cursor: first_page_metadata["after"])

      assert second_page_books = json_response(conn, 200)["entries"]
      assert second_page_metadata = json_response(conn, 200)["metadata"]
      assert length(first_page_books) == first_page_metadata["limit"]

      {:ok, first_page_book_updated_at, 0} =
        DateTime.from_iso8601(List.first(first_page_books)["updated_at"])

      {:ok, second_page_book_updated_at, 0} =
        DateTime.from_iso8601(List.first(second_page_books)["updated_at"])

      assert DateTime.diff(
               first_page_book_updated_at,
               second_page_book_updated_at,
               :millisecond
             ) >= 5000
    end

    for readind_status_id <- [
          ReadingStatus.id_reading(),
          ReadingStatus.id_plan_to_read(),
          ReadingStatus.id_completed()
        ] do
      test "lists books response with readind_status filter #{readind_status_id}", %{
        conn: conn,
        user: user
      } do
        readind_status_id = unquote(readind_status_id)
        user_storage_provider = insert(:user_storage_provider, user: user)

        insert_list(15, :book,
          user_storage_provider: user_storage_provider,
          reading_status_id: ReadingStatus.id_unknown()
        )

        insert_list(15, :book,
          user_storage_provider: user_storage_provider,
          reading_status_id: readind_status_id
        )

        # Test without filters
        conn = get(conn, Routes.book_path(conn, :index))

        assert books = json_response(conn, 200)["entries"]
        assert metadata = json_response(conn, 200)["metadata"]

        for book <- books do
          assert book["reading_status_id"] in [readind_status_id, ReadingStatus.id_unknown()]
        end

        # Test with one reading_status
        conn = get(conn, Routes.book_path(conn, :index), reading_status_id: readind_status_id)

        assert books = json_response(conn, 200)["entries"]
        assert metadata = json_response(conn, 200)["metadata"]

        for book <- books do
          assert book["reading_status_id"] == readind_status_id
        end

        # Test with multiple reading_status
        conn =
          get(conn, Routes.book_path(conn, :index),
            reading_status_id: [readind_status_id, ReadingStatus.id_unknown()]
          )

        assert books = json_response(conn, 200)["entries"]
        assert metadata = json_response(conn, 200)["metadata"]

        for book <- books do
          assert book["reading_status_id"] in [readind_status_id, ReadingStatus.id_unknown()]
        end
      end
    end

    test "lists paginated books response with readind_status filter", %{
      conn: conn,
      user: user
    } do
      user_storage_provider = insert(:user_storage_provider, user: user)

      insert_list(30, :book,
        user_storage_provider: user_storage_provider,
        reading_status_id: ReadingStatus.id_reading()
      )

      insert_list(30, :book,
        user_storage_provider: user_storage_provider,
        reading_status_id: ReadingStatus.id_unknown(),
        updated_at: DateTime.add(DateTime.utc_now(), 5, :second)
      )

      conn =
        get(conn, Routes.book_path(conn, :index), reading_status_id: ReadingStatus.id_reading())

      assert first_page_books = json_response(conn, 200)["entries"]
      assert first_page_metadata = json_response(conn, 200)["metadata"]

      for book <- first_page_books do
        assert book["reading_status_id"] == ReadingStatus.id_reading()
      end

      conn =
        get(conn, Routes.book_path(conn, :index),
          reading_status_id: ReadingStatus.id_reading(),
          next_page_cursor: first_page_metadata["after"]
        )

      assert second_page_books = json_response(conn, 200)["entries"]
      assert second_page_metadata = json_response(conn, 200)["metadata"]

      for book <- second_page_books do
        assert book["reading_status_id"] == ReadingStatus.id_reading()
      end
    end
  end

  describe "show" do
    setup [:create_authenticated_conn]

    test "show clients's book", %{
      conn: conn,
      user: user
    } do
      user_storage_provider = insert(:user_storage_provider, user: user)
      %{id: book_id} = _book = insert(:book, user_storage_provider: user_storage_provider)

      conn = get(conn, Routes.book_path(conn, :show, book_id))
      assert resp_book = json_response(conn, 200)["data"]
      assert %{"id" => ^book_id} = resp_book
    end

    test "don't show other client's book", %{conn: conn} do
      ticket = insert(:book)
      conn = get(conn, Routes.book_path(conn, :show, ticket.id))
      assert _resp_book = json_response(conn, 404)["errors"]
    end
  end

  describe "update book" do
    setup [:create_authenticated_conn, :create_book]

    test "update current_page", %{conn: conn, book: %Book{id: id} = book} do
      conn = put(conn, Routes.book_path(conn, :update, book), book: %{current_page: 9})
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.book_path(conn, :show, id))

      assert %{
               "current_page" => 9
             } = json_response(conn, 200)["data"]
    end

    test "update readind_status", %{conn: conn, book: %Book{id: id} = book} do
      conn =
        put(conn, Routes.book_path(conn, :update, book),
          book: %{reading_status_id: ReadingStatus.id_completed()}
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.book_path(conn, :show, id))
      id_completed = ReadingStatus.id_completed()

      assert %{
               "reading_status_id" => ^id_completed
             } = json_response(conn, 200)["data"]
    end
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_conn(conn)
  end

  defp create_book(%{user: user}) do
    user_storage_provider = insert(:user_storage_provider, user: user)

    book =
      insert(:book,
        user_storage_provider: user_storage_provider,
        reading_status_id: ReadingStatus.id_unknown()
      )

    %{book: book, user_storage_provider: user_storage_provider}
  end
end
