defmodule CloudBooksWeb.AccountControllerTest do
  use CloudBooksWeb.ConnCase
  alias CloudBooks.{Accounts, TestHelper}
  alias CloudBooksWeb.Helper.WebSocketTokenHelper

  @valid_attrs %{
    name: "waldo garcia",
    email: "test@mail.com",
    password: "some password",
    is_admin_account_created: true
  }
  @valid_login_attrs %{email: "test@mail.com", password: "some password"}
  @invalid_login_attrs %{email: "test@mail.com", password: "some wrong password"}

  def fixture(:user) do
    {:ok, user} = Accounts.register_user(@valid_attrs)
    user
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_conn(conn)
  end

  describe "login" do
    setup [:create_user]

    test "with invalid credentials, wrong password", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :login), user: @invalid_login_attrs)
      response_data = json_response(conn, 422)["errors"]
      assert %{"email" => _email_errors} = response_data
    end

    test "with invalid credentials, wrong email", %{conn: conn} do
      conn =
        post(conn, Routes.account_path(conn, :login),
          user: %{email: "test123@mail.com", password: "some wrong password"}
        )

      assert response_data = json_response(conn, 422)["errors"]
      assert %{"email" => _email_errors} = response_data
    end

    test "with valid credentials", %{
      conn: conn
    } do
      conn = post(conn, Routes.account_path(conn, :login), user: @valid_login_attrs)
      assert response_data = json_response(conn, 200)

      assert %{
               "user" => %{
                 "email" => "test@mail.com",
                 "name" => "waldo garcia"
               }
             } = response_data

      assert %{"current_session_key" => session_id} = get_session(conn)
    end

    test "with upcase email", %{conn: conn} do
      conn =
        post(conn, Routes.account_path(conn, :login),
          user: %{@valid_attrs | email: String.upcase(@valid_attrs.email)}
        )

      assert conn.status == 200
    end

    test "check valid login's last date", %{conn: conn, user: user} do
      conn = post(conn, Routes.account_path(conn, :login), user: @valid_attrs)
      assert conn.status == 200
      logged_user = Accounts.get_user!(user.id)
      assert DateTime.diff(DateTime.utc_now(), logged_user.last_login, :millisecond) < 1500
    end
  end

  describe "signup" do
    test "create user with valid params", %{conn: conn} do
      params = %{
        user: %{
          name: "walter white",
          email: "test@win.com",
          password: "abcpassword"
        }
      }

      conn = post(conn, Routes.account_path(conn, :signup), params)
      response_data = json_response(conn, 201)
      assert response_data["user"]["email"] == "test@win.com"
    end

    test "try with a duplicated email", %{conn: conn} do
      {:ok, _} = Accounts.register_user(@valid_attrs)

      params = %{
        user: @valid_attrs
      }

      conn = post(conn, Routes.account_path(conn, :signup), params)
      response_data_errors = json_response(conn, 422)["errors"]

      assert %{
               "email" => email_errors
             } = response_data_errors

      assert length(email_errors) > 0
    end
  end

  describe "logout" do
    setup [:create_authenticated_conn]

    test "with valid credentials", %{conn: conn} do
      assert %{"current_session_key" => _} = get_session(conn)
      conn = post(conn, Routes.account_path(conn, :logout))
      assert response(conn, 204)
      assert get_session(conn) == %{}
    end
  end

  describe "socket token" do
    setup [:create_authenticated_conn]

    test "get token", %{conn: conn, session: session} do
      conn = get(conn, Routes.account_path(conn, :get_socket_token))
      response = json_response(conn, 200)["data"]

      assert %{
               "token" => token,
               "user_id" => user_id
             } = response

      session_id = session.id
      assert user_id == session.user_id
      assert {:ok, ^session_id} = WebSocketTokenHelper.verify_token(conn, token)
    end
  end
end
