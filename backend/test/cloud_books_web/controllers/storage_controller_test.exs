defmodule CloudBooksWeb.StorageControllerTest do
  use CloudBooksWeb.ConnCase
  import CloudBooks.Factory

  alias CloudBooks.TestHelper

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_conn(conn)
  end

  describe "delete storage" do
    setup [:create_authenticated_conn]

    test "deletes chosen storage", %{conn: conn, user: user} do
      user_storage_provider = insert(:user_storage_provider, user: user)
      insert_list(5, :book, user_storage_provider: user_storage_provider)

      conn = delete(conn, Routes.storage_path(conn, :delete, user_storage_provider.id))
      assert response(conn, 204)

      # assert_error_sent 404, fn ->
      #   get(conn, Routes.book_path(conn, :show, book))
      # end
    end
  end
end
