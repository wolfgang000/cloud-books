defmodule CloudBooksWeb.CookieAuthPlugTest do
  use CloudBooksWeb.ConnCase
  use Plug.Test
  import CloudBooks.Factory
  alias CloudBooksWeb.CookieAuthPlug

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "should succeed with right session_key", %{conn: conn} do
    %{user: user} = session = insert(:session)

    conn =
      conn
      |> Plug.Test.init_test_session(%{})
      |> put_status(200)
      |> Plug.Conn.put_session(:current_session_key, session.session_key)
      |> CookieAuthPlug.call({})

    assert conn.status == 200
    assert %{assigns: %{current_user: assign_user}} = conn
    assert assign_user.id == user.id
  end

  test "should fail with expired session", %{conn: conn} do
    session =
      insert(:session, expiration_date: DateTime.add(DateTime.utc_now(), -(60 * 15), :second))

    conn =
      conn
      |> Plug.Test.init_test_session(%{})
      |> put_status(200)
      |> Plug.Conn.put_session(:current_session_key, session.session_key)
      |> CookieAuthPlug.call({})

    assert conn.status == 401
  end

  test "should fail with wrong session_id", %{conn: conn} do
    conn =
      conn
      |> Plug.Test.init_test_session(%{})
      |> put_status(200)
      |> Plug.Conn.put_session(:current_session_key, "test")
      |> CookieAuthPlug.call({})

    assert conn.status == 401
  end
end
