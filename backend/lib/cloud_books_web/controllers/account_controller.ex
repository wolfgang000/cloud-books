defmodule CloudBooksWeb.AccountController do
  use CloudBooksWeb, :controller

  alias CloudBooks.{Accounts, Email, Mailer}
  alias CloudBooksWeb.Helper.WebSocketTokenHelper

  action_fallback CloudBooksWeb.FallbackController

  # TODO: replace session.id with a random session_id
  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    with {:ok, user} <- Accounts.authenticate_user(String.downcase(email), password),
         {:ok, user} <- Accounts.update_user_last_login_date(user),
         {:ok, session} <- Accounts.create_session(%{user_id: user.id}) do
      token = WebSocketTokenHelper.sign_data(conn, session.id)

      conn
      |> put_session(:current_session_key, session.session_key)
      |> put_status(:ok)
      |> render("success_login.json", user: user, web_socket_token: token)
    end
  end

  def signup(
        conn,
        %{"user" => user_params}
      ) do
    with {:ok, user} <- Accounts.register_user(user_params) do
      # TODO: move the welcome email to mailchimp
      Email.sign_in_email(user.email, %{
        user_name: user.name,
        app_url: CloudBooksWeb.Endpoint.url()
      })
      |> Mailer.deliver_later()

      conn
      |> put_status(:created)
      |> render("successful_signup.json", user: user)
    end
  end

  def logout(%{assigns: %{current_session_id: session_id}} = conn, _params) do
    with {:ok, _} <- Accounts.delete_session_by_id(session_id) do
      conn
      |> clear_session()
      |> send_resp(204, "")
    end
  end

  def get_socket_token(
        %{assigns: %{current_session_id: session_id, current_user: %{id: user_id}}} = conn,
        _
      ) do
    token = WebSocketTokenHelper.sign_data(conn, session_id)
    render(conn, "show_user_socket_token.json", token: token, user_id: user_id)
  end
end
