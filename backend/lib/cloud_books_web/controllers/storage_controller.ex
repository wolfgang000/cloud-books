defmodule CloudBooksWeb.StorageController do
  use CloudBooksWeb, :controller

  require Logger
  alias CloudBooks.Accounts
  alias CloudBooks.Accounts.UserStorageProvider
  alias CloudBooks.Helper.S3Helper

  action_fallback CloudBooksWeb.FallbackController

  def index(%{assigns: %{current_user: %{id: user_id}}} = conn, _params) do
    books = Accounts.list_users_storage_providers(user_id)
    render(conn, "index.json", storage: books)
  end

  def sync(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"id" => id}) do
    with {:ok, %UserStorageProvider{is_synchronizing: false}} <-
           Accounts.get_users_storage_provider(id, user_id) do
      CloudBooks.Helper.SyncManager.sync(id)

      conn
      |> send_resp(200, "")
    else
      # TODO: handle this case with an appropriate message on the fronend
      {:ok, _} ->
        conn
        |> send_resp(200, "")

      e ->
        e
    end
  end

  def delete(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"id" => id}) do
    with {:ok, %UserStorageProvider{} = user_storage_provider} <-
           Accounts.get_users_storage_provider(id, user_id),
         {:ok, _} <- Accounts.delete_users_storage_provider(user_storage_provider) do
      Task.start(fn ->
        case S3Helper.delete_all_objects("#{id}") do
          {:ok, _} ->
            :ok

          error ->
            Logger.error(
              "Unexpected error while delete user_storage_provider files id: #{id}, user_id: #{
                user_id
              }, error: #{inspect(error)}"
            )
        end
      end)

      send_resp(conn, :no_content, "")
    end
  end
end
