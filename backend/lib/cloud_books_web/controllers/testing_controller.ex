defmodule CloudBooksWeb.TestingController do
  use CloudBooksWeb, :controller
  import Ecto.Query, warn: false
  alias CloudBooks.Repo

  alias CloudBooks.Core.Book
  alias CloudBooks.Accounts

  alias CloudBooks.Accounts.{
    Session,
    User,
    UserStorageProvider
  }

  action_fallback CloudBooksWeb.FallbackController

  def reset_data(conn, _) do
    Repo.delete_all(Book)
    Repo.delete_all(Session)
    Repo.delete_all(UserStorageProvider)
    Repo.delete_all(User)

    conn
    |> send_resp(200, "")
  end

  def create_user_storage_provider(conn, %{
        "user_storage_provider" => user_storage_provider_params
      }) do
    with {:ok, _} <- Accounts.create_user_storage_provider(user_storage_provider_params) do
      conn
      |> send_resp(200, "")
    end
  end
end
