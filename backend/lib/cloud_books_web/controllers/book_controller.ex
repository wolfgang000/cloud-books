defmodule CloudBooksWeb.BookController do
  use CloudBooksWeb, :controller

  require Logger
  alias CloudBooks.Core
  alias CloudBooks.Core.Book
  alias CloudBooks.Helper.{BookQueryHelper, S3Helper}

  action_fallback CloudBooksWeb.FallbackController

  def storage_provider_api_helper,
    do: Application.fetch_env!(:cloud_books, :storage_provider_api_helper)

  def index(
        %{assigns: %{current_user: %{id: user_id}}} = conn,
        %{"next_page_cursor" => next_page_cursor} = params
      ) do
    paginated_result =
      BookQueryHelper.get_user_books_query(user_id)
      |> BookQueryHelper.filter_by_reading_status_id(params)
      |> BookQueryHelper.paginate_by_updated_at_and_id_with_page_cursor(next_page_cursor)

    render(conn, "paginated_index.json", Map.from_struct(paginated_result))
  end

  def index(%{assigns: %{current_user: %{id: user_id}}} = conn, params) do
    paginated_result =
      BookQueryHelper.get_user_books_query(user_id)
      |> BookQueryHelper.filter_by_reading_status_id(params)
      |> BookQueryHelper.paginate_by_updated_at_and_id()

    render(conn, "paginated_index.json", Map.from_struct(paginated_result))
  end

  def show(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"id" => id}) do
    with {:ok, %Book{} = book} <- Core.get_book_with_preloaded_data(id, user_id) do
      render(conn, "show.json", book: book)
    end
  end

  def update(%{assigns: %{current_user: %{id: user_id}}} = conn, %{
        "id" => id,
        "book" => book_params
      }) do
    with {:ok, %Book{} = book} <- Core.get_book(id, user_id),
         {:ok, %Book{} = book} <- Core.update_book(book, book_params) do
      render(conn, "show.json", book: book)
    end
  end

  def loop_receive(%HTTPoison.AsyncResponse{id: resp_id} = resp, conn) do
    receive do
      %HTTPoison.AsyncStatus{code: _status_code, id: ^resp_id} ->
        # TODO: check this status
        HTTPoison.stream_next(resp)
        loop_receive(resp, conn)

      %HTTPoison.AsyncHeaders{headers: headers, id: ^resp_id} ->
        content_length = :proplists.get_value("Content-Length", headers)
        content_type = :proplists.get_value("Content-Type", headers)

        updated_conn =
          conn
          |> put_resp_header("content-length", content_length)
          |> put_resp_header("content-type", content_type)
          |> put_resp_header("cache-control", "max-age=2628000")
          |> send_chunked(200)

        HTTPoison.stream_next(resp)
        loop_receive(resp, updated_conn)

      %HTTPoison.AsyncChunk{chunk: chunk, id: ^resp_id} ->
        {:ok, updated_conn} = chunk(conn, chunk)
        HTTPoison.stream_next(resp)
        loop_receive(resp, updated_conn)

      %HTTPoison.AsyncEnd{id: ^resp_id} ->
        conn
    end
  end

  # TODO: add support for content-range to speed up downloads
  # https://developers.google.com/drive/api/v3/manage-downloads#partial_download
  def get_file(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"id" => id}) do
    with {:ok, %Book{} = book} <- Core.get_book_with_preloaded_data(id, user_id),
         {:ok, token} <-
           CloudBooks.Helper.TokenManager.get_access_token(book.user_storage_provider_id) do
      asyn_req = storage_provider_api_helper().get_file(token, book.file_id)
      loop_receive(asyn_req, conn)
    end
  end

  def get_thumbnail(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"id" => id}) do
    with {:ok, %Book{} = book} <- Core.get_book(id, user_id),
         {:get_presigned_url, {:ok, thumbnail_presigned_url}} <-
           {:get_presigned_url, S3Helper.get_presigned_url(:get, book.thumbnail_s3_object_key)} do
      asyn_req =
        HTTPoison.get!(thumbnail_presigned_url, [],
          stream_to: self(),
          async: :once,
          recv_timeout: 15000
        )

      loop_receive(asyn_req, conn)
    else
      {:get_presigned_url, error} ->
        Logger.error("Unable to generate get_presigned_url for book #{id}, error: #{error}")

      e ->
        e
    end
  end
end
