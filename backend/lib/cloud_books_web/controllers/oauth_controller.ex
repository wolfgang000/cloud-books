defmodule CloudBooksWeb.OAuthController do
  use CloudBooksWeb, :controller
  require Logger
  alias CloudBooks.Accounts.StorageProvider
  alias CloudBooks.Accounts

  action_fallback CloudBooksWeb.FallbackController

  @storage_provider_id_google_drive StorageProvider.storage_provider_id_google_drive()
  @storage_provider_id_dropbox StorageProvider.storage_provider_id_dropbox()

  def google_oauth, do: Application.fetch_env!(:cloud_books, :google_oauth)
  def dropbox_oauth, do: Application.fetch_env!(:cloud_books, :dropbox_oauth)

  def get_oauth(@storage_provider_id_google_drive), do: google_oauth()
  def get_oauth(@storage_provider_id_dropbox), do: dropbox_oauth()

  def storage_provider_api_helper,
    do: Application.fetch_env!(:cloud_books, :storage_provider_api_helper)

  defp callback_handler(conn, user_id, storage_provider_id, code) do
    with {:get_access_token, {:ok, client}} <-
           {:get_access_token, get_oauth(storage_provider_id).get_access_token(code: code)},
         {:get_account_info, {:ok, %{id: account_id, name: account_name}}} <-
           {:get_account_info,
            storage_provider_api_helper().get_account_info(%CloudBooks.StorageProviderToken{
              storage_provider_id: storage_provider_id,
              access_token: client.token.access_token
            })},
         {:create_user_storage_provider, {:ok, _user_storage_provider}} <-
           {:create_user_storage_provider,
            Accounts.create_user_storage_provider(%{
              account_id: account_id,
              account_name: account_name,
              refresh_token: client.token.refresh_token,
              storage_provider_id: storage_provider_id,
              user_id: user_id
            })} do
      conn
      |> redirect(external: "/home/books/storage/#account_successfully_added")
    else
      {:get_access_token, error} ->
        Logger.error("Unable to get access_token from code #{inspect(error)}")

        conn
        |> redirect(external: "/home/books/storage/#unable_to_add_account")

      {:get_account_info, error} ->
        Logger.error("Unable to get account_id #{inspect(error)}")

        conn
        |> redirect(external: "/home/books/storage/#unable_to_add_account")

      {:create_user_storage_provider,
       {:error,
        %Ecto.Changeset{
          changes: %{
            account_id: account_id,
            storage_provider_id: storage_provider_id,
            user_id: user_id
          },
          errors: [
            account_id:
              {_,
               [
                 constraint: :unique,
                 constraint_name: "user_storage_provider_account_id_storage_provider_id_index"
               ]}
          ]
        }}} ->
        case Accounts.get_users_storage_provider_by_clauses(
               account_id: account_id,
               storage_provider_id: storage_provider_id,
               user_id: user_id
             ) do
          {:error, :not_found} ->
            # TODO: show appropriate message
            Logger.error(
              "account associated to another account_id: #{account_id}, storage_provider_id: #{
                storage_provider_id
              }"
            )

            conn
            |> redirect(external: "/home/books/storage/#unable_to_add_account")

          {:ok, _} ->
            conn
            |> redirect(external: "/home/books/storage/#account_already_added")
        end

      {:create_user_storage_provider, error} ->
        Logger.error("Unable to create user_storage_provider #{inspect(error)}")

        conn
        |> redirect(external: "/home/books/storage/#unable_to_add_account")
    end
  end

  def google_authorize_url(conn, _params) do
    authorize_url = google_oauth().get_authorize_url()

    conn
    |> redirect(external: authorize_url)
  end

  def google_callback(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"code" => code}) do
    callback_handler(conn, user_id, @storage_provider_id_google_drive, code)
  end

  def google_callback(conn, _params) do
    conn
    |> redirect(external: "/home/books/storage/#unable_to_add_account")
  end

  def dropbox_authorize_url(conn, _params) do
    authorize_url = dropbox_oauth().get_authorize_url()

    conn
    |> redirect(external: authorize_url)
  end

  def dropbox_callback(%{assigns: %{current_user: %{id: user_id}}} = conn, %{"code" => code}) do
    callback_handler(conn, user_id, @storage_provider_id_dropbox, code)
  end

  def dropbox_callback(conn, _params) do
    conn
    |> redirect(external: "/home/books/storage/#unable_to_add_account")
  end
end
