defmodule CloudBooksWeb.UserSocket do
  use Phoenix.Socket
  alias CloudBooksWeb.Helper.WebSocketTokenHelper
  alias CloudBooks.Accounts

  channel "user:*", CloudBooksWeb.UserChannel

  @impl true
  def connect(%{"token" => token}, socket, _connect_info) do
    with {:ok, session_id} <- WebSocketTokenHelper.verify_token(socket, token, max_age: 86400),
         {:ok, %{user: user} = session} <-
           Accounts.get_session_by_id_with_preloaded_data(session_id) do
      socket =
        socket
        |> assign(:current_user, user)
        |> assign(:current_user_id, user.id)
        |> assign(:current_session_id, session.id)

      {:ok, socket}
    else
      {:error, _reason} ->
        :error

      _ ->
        :error
    end
  end

  def connect(_params, _socket, _connect_info), do: :error

  @impl true
  def id(socket), do: "users_socket:#{socket.assigns.current_user_id}"
end
