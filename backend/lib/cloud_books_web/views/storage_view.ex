defmodule CloudBooksWeb.StorageView do
  use CloudBooksWeb, :view
  alias CloudBooksWeb.StorageView

  def render("index.json", %{storage: storage}) do
    %{data: render_many(storage, StorageView, "storage.json")}
  end

  def render("show.json", %{storage: storage}) do
    %{data: render_one(storage, StorageView, "storage.json")}
  end

  def render("storage.json", %{storage: storage}) do
    %{
      id: storage.id,
      account_name: storage.account_name,
      storage_provider_id: storage.storage_provider_id,
      is_synchronizing: storage.is_synchronizing,
      last_started_at: storage.last_started_at,
      last_ended_at: storage.last_ended_at
    }
  end
end
