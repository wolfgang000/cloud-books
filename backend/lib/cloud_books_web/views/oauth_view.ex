defmodule CloudBooksWeb.OAuthView do
  use CloudBooksWeb, :view
  alias CloudBooksWeb.OAuthView

  def render("show_google_authorize_url.json", %{authorize_url: authorize_url}) do
    %{data: render_one(authorize_url, OAuthView, "google_authorize_url.json")}
  end

  def render("google_authorize_url.json", %{o_auth: authorize_url}) do
    %{
      authorize_url: authorize_url
    }
  end
end
