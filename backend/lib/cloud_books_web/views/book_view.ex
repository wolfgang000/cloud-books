defmodule CloudBooksWeb.BookView do
  use CloudBooksWeb, :view
  alias CloudBooksWeb.BookView

  def render("index.json", %{books: books}) do
    %{data: render_many(books, BookView, "book.json")}
  end

  def render("paginated_index.json", %{entries: entries, metadata: metadata}) do
    %{
      entries: render_many(entries, BookView, "book.json"),
      metadata: render_one(metadata, BookView, "metadata.json")
    }
  end

  def render("show.json", %{book: book}) do
    %{data: render_one(book, BookView, "book.json")}
  end

  def render("book.json", %{book: book}) do
    %{
      id: book.id,
      title: book.title,
      file_name: book.file_name,
      author: book.author,
      current_page: book.current_page,
      num_pages: book.num_pages,
      thumbnail_url:
        CloudBooksWeb.Router.Helpers.book_path(CloudBooksWeb.Endpoint, :get_thumbnail, book.id),
      reading_status_id: book.reading_status_id,
      inserted_at: book.inserted_at,
      updated_at: book.updated_at
    }
  end

  def render("metadata.json", %{book: metadata}) do
    %{
      after: metadata.after,
      limit: metadata.limit
    }
  end
end
