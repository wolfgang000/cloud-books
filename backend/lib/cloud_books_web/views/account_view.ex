defmodule CloudBooksWeb.AccountView do
  use CloudBooksWeb, :view
  alias CloudBooksWeb.AccountView

  def render("success_login.json", %{user: user}) do
    %{user: render_one(user, AccountView, "login_user.json")}
  end

  def render("login_user.json", %{account: login_user}) do
    %{
      id: login_user.id,
      name: login_user.name,
      email: login_user.email
    }
  end

  def render("successful_signup.json", %{user: user}) do
    %{
      user: render_one(user, AccountView, "user.json")
    }
  end

  def render("user.json", %{account: user}) do
    %{
      id: user.id,
      email: user.email,
      name: user.name
    }
  end

  def render("show_user_socket_token.json", params) do
    %{data: render_one(params, AccountView, "user_socket_token.json")}
  end

  def render("user_socket_token.json", %{account: %{token: token, user_id: user_id}}) do
    %{token: token, user_id: user_id}
  end
end
