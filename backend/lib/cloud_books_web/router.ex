defmodule CloudBooksWeb.Router do
  use CloudBooksWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:put_secure_browser_headers)
  end

  pipeline :cookie_auth do
    plug CloudBooksWeb.CookieAuthPlug
  end

  scope "/api", CloudBooksWeb do
    pipe_through [:api, :cookie_auth]

    post "/accounts/logout", AccountController, :logout
    get "/accounts/web_socket_token", AccountController, :get_socket_token
    get "/accounts/oauth/google_callback", OAuthController, :google_callback
    get "/accounts/oauth/google_authorize_url", OAuthController, :google_authorize_url
    get "/accounts/oauth/dropbox_callback", OAuthController, :dropbox_callback
    get "/accounts/oauth/dropbox_authorize_url", OAuthController, :dropbox_authorize_url

    resources "/books", BookController, only: [:index, :show, :update]
    get "/books/:id/file", BookController, :get_file
    get "/books/:id/thumbnail", BookController, :get_thumbnail

    resources "/storage", StorageController, only: [:index, :delete]
    post "/storage/:id/sync", StorageController, :sync
  end

  scope "/api/accounts", CloudBooksWeb do
    pipe_through [:api]

    post "/login", AccountController, :login
    post "/signup", AccountController, :signup
  end

  ######################
  forward(
    "/api/health",
    PlugCheckup,
    PlugCheckup.Options.new(
      json_encoder: Jason,
      checks: [
        %PlugCheckup.Check{name: "DB", module: CloudBooksWeb.Healthchecks, function: :check_db}
      ]
    )
  )

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/api", CloudBooksWeb do
      pipe_through :api

      delete "/testing/reset_data", TestingController, :reset_data
      post "/testing/user_storage_provider", TestingController, :create_user_storage_provider
    end

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: CloudBooksWeb.Telemetry
    end

    forward "/api/sent_emails", Bamboo.SentEmailViewerPlug
  end
end
