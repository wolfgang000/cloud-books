defmodule CloudBooksWeb.CookieAuthPlug do
  import Plug.Conn
  alias CloudBooks.Accounts

  @doc false
  def init(default), do: default

  @doc false
  def call(%Plug.Conn{} = conn, _options) do
    with current_session_key when not is_nil(current_session_key) <-
           get_session(conn, :current_session_key),
         {:ok,
          %{
            user: user
          } = session} <-
           Accounts.get_session_by_key_with_preloaded_data(current_session_key),
         :ok <- check_expiration_date(session) do
      conn
      |> assign(:current_user, user)
      |> assign(:current_session, session)
      |> assign(:current_session_id, session.id)
    else
      {:error, :expired_session, session} ->
        Accounts.delete_session_by_id(session.id)

        conn
        |> clear_session()
        |> unauthorized()

      _ ->
        unauthorized(conn)
    end
  end

  defp check_expiration_date(session) do
    if DateTime.diff(session.expiration_date, DateTime.utc_now()) > 0,
      do: :ok,
      else: {:error, :expired_session, session}
  end

  defp unauthorized(conn) do
    conn
    |> send_resp(401, "")
    |> halt()
  end
end
