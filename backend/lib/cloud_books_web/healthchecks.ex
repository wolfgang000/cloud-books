defmodule CloudBooksWeb.Healthchecks do
  alias CloudBooks.Repo

  def check_db do
    case Repo.query("SELECT 1") do
      {:ok, _} -> :ok
      _ -> {:error, "unable to connect to db"}
    end
  end
end
