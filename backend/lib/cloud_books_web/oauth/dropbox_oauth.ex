defmodule CloudBooksWeb.DropboxOAuth do
  use OAuth2.Strategy
  @behaviour CloudBooksWeb.OAuthBehaviour

  @scopes "files.content.read account_info.read"

  def client do
    OAuth2.Client.new(
      strategy: __MODULE__,
      client_id: Application.get_env(:oauth2, :dropbox_client_id),
      client_secret: Application.get_env(:oauth2, :dropbox_client_secret),
      redirect_uri: Application.get_env(:oauth2, :dropbox_redirect_uri),
      site: "https://api.dropbox.com",
      authorize_url: "https://www.dropbox.com/oauth2/authorize",
      token_url: "https://api.dropbox.com/oauth2/token"
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  def authorize_url!(params \\ []) do
    OAuth2.Client.authorize_url!(client(), params)
  end

  def get_authorize_url(),
    do:
      OAuth2.Client.authorize_url!(client(),
        scope: @scopes,
        token_access_type: "offline"
      )

  defp delete_param(%{params: params} = client, key) do
    %{client | params: Map.delete(params, "#{key}")}
  end

  def get_access_token(params \\ [], headers \\ [], opts \\ []) do
    client()
    |> OAuth2.Client.get_token(params, headers, opts)
  end

  # Strategy Callbacks
  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers) do
    client
    |> put_header("accept", "application/json")
    |> OAuth2.Strategy.AuthCode.get_token(params, headers)
    |> delete_param(:client_id)
  end

  def get_refresh_token(refresh_token) do
    client()
    |> Map.put(:token, %{refresh_token: refresh_token})
    |> OAuth2.Client.refresh_token()
  end
end
