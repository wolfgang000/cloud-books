defmodule CloudBooksWeb.GoogleOAuth do
  use OAuth2.Strategy
  @behaviour CloudBooksWeb.OAuthBehaviour

  @scopes "https://www.googleapis.com/auth/drive.readonly"

  def client do
    OAuth2.Client.new(
      strategy: __MODULE__,
      client_id: Application.get_env(:oauth2, :google_client_id),
      client_secret: Application.get_env(:oauth2, :google_client_secret),
      redirect_uri: Application.get_env(:oauth2, :google_redirect_uri),
      site: "https://accounts.google.com",
      authorize_url: "/o/oauth2/auth",
      token_url: "/o/oauth2/token"
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  def authorize_url!(params \\ []) do
    OAuth2.Client.authorize_url!(client(), params)
  end

  def get_authorize_url(),
    do:
      OAuth2.Client.authorize_url!(client(),
        scope: @scopes,
        access_type: "offline",
        prompt: "consent"
      )

  def get_access_token(params \\ [], headers \\ [], opts \\ []) do
    OAuth2.Client.get_token(client(), params, headers, opts)
  end

  # Strategy Callbacks
  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers) do
    client
    |> put_param(:client_secret, client.client_secret)
    |> put_header("accept", "application/json")
    |> OAuth2.Strategy.AuthCode.get_token(params, headers)
  end

  def get_refresh_token(refresh_token) do
    client()
    |> Map.put(:token, %{refresh_token: refresh_token})
    |> OAuth2.Client.refresh_token()
  end
end
