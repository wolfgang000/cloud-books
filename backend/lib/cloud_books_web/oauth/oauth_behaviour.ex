defmodule CloudBooksWeb.OAuthBehaviour do
  @moduledoc false
  @type headers :: [{binary, binary}]
  @type param :: binary | %{binary => param} | [param]
  @type params :: %{binary => param} | Keyword.t()
  @type get_access_token_resp ::
          {:ok, OAuth2.Client.t()} | {:error, OAuth2.Response.t()} | {:error, OAuth2.Error.t()}

  @callback get_access_token(params, headers, Keyword.t()) :: get_access_token_resp
  @callback get_access_token(params, headers) :: get_access_token_resp
  @callback get_access_token(params) :: get_access_token_resp
  @callback get_refresh_token(binary) :: get_access_token_resp
  @callback get_authorize_url() :: binary()
end
