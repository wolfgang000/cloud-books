defmodule CloudBooksWeb.Helper.WebSocketTokenHelper do
  @auth_namespace "websocket auth"

  def sign_data(context, data) do
    Phoenix.Token.sign(context, @auth_namespace, data)
  end

  def verify_token(context, token, opts \\ [{:max_age, 3600}]) do
    Phoenix.Token.verify(context, @auth_namespace, token, opts)
  end
end
