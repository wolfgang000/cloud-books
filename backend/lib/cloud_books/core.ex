defmodule CloudBooks.Core do
  @moduledoc """
  The Core context.
  """

  import Ecto.Query, warn: false
  alias Ecto.Adapters.SQL
  alias CloudBooks.{Repo, Util}
  alias CloudBooks.Core.Book

  @doc """
  Returns the list of books.

  ## Examples

      iex> list_books()
      [%Book{}, ...]

  """
  def list_books do
    Repo.all(Book)
  end

  @doc """
  Gets a single book.

  Raises `Ecto.NoResultsError` if the Book does not exist.

  ## Examples

      iex> get_book!(123)
      %Book{}

      iex> get_book!(456)
      ** (Ecto.NoResultsError)

  """
  def get_book!(id), do: Repo.get!(Book, id)

  @doc """
  Creates a book.

  ## Examples

      iex> create_book(%{field: value})
      {:ok, %Book{}}

      iex> create_book(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_book(attrs \\ %{}) do
    %Book{}
    |> Book.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a book.

  ## Examples

      iex> update_book(book, %{field: new_value})
      {:ok, %Book{}}

      iex> update_book(book, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_book(%Book{} = book, attrs) do
    book
    |> Book.changeset_update(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a book.

  ## Examples

      iex> delete_book(book)
      {:ok, %Book{}}

      iex> delete_book(book)
      {:error, %Ecto.Changeset{}}

  """
  def delete_book(%Book{} = book) do
    Repo.delete(book)
  end

  def delete_books(user_storage_provider_id) do
    from(b in Book, where: b.user_storage_provider_id == ^user_storage_provider_id)
    |> Repo.delete_all()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking book changes.

  ## Examples

      iex> change_book(book)
      %Ecto.Changeset{data: %Book{}}

  """
  def change_book(%Book{} = book, attrs \\ %{}) do
    Book.changeset(book, attrs)
  end

  def get_book_with_preloaded_data(id, user_id) do
    from(b in Book,
      join: usp in assoc(b, :user_storage_provider),
      preload: [user_storage_provider: usp],
      where: b.id == ^id,
      where: usp.user_id == ^user_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_book(id, user_id) do
    from(b in Book,
      join: usp in assoc(b, :user_storage_provider),
      where: b.id == ^id,
      where: usp.user_id == ^user_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_book_by_file_id_and_user_storage_provider_id(file_id, user_storage_provider_id) do
    from(b in Book,
      where: b.file_id == ^file_id,
      where: b.user_storage_provider_id == ^user_storage_provider_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_file_ids_not_present_in_users_books(file_ids, user_storage_provider_id) do
    result =
      SQL.query!(
        Repo,
        """
        with book_search as (
          SELECT unnest($1::varchar[]) file_id
        ), book_user as (
          SELECT * from books as b where b.user_storage_provider_id = $2::integer
        ) select bs.*
        from book_search as bs
        left join book_user as b on bs.file_id = b.file_id
        where b.file_id is null
        """,
        [
          file_ids,
          user_storage_provider_id
        ]
      )

    Enum.map(result.rows, fn [file_id] -> file_id end)
  end
end
