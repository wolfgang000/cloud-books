defmodule CloudBooks.Email do
  use Bamboo.Phoenix, view: CloudBooksWeb.EmailView

  def sign_in_email(recipient, params) do
    from = Application.get_env(:cloud_books, CloudBooks.Mailer)[:default_from_address]

    base_email()
    |> from(from)
    |> to(recipient)
    |> subject("Hello from Parcebooks")
    |> render("signup.html", params)
  end

  defp base_email do
    new_email()
    |> put_html_layout({CloudBooksWeb.LayoutView, "email.html"})
  end
end
