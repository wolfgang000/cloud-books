defmodule CloudBooks.Util do
  def return_value_or_not_found(v) do
    case v do
      nil -> {:error, :not_found}
      value -> {:ok, value}
    end
  end

  def poison_async_stream_to_file(
        %HTTPoison.AsyncResponse{} = resp,
        file_pid
      ) do
    poison_async_stream_to_file_handler(resp, file_pid)
  end

  @spec poison_async_stream_to_file_handler(HTTPoison.AsyncResponse.t(), File.io_device(), map()) ::
          :ok | {:error, any}
  defp poison_async_stream_to_file_handler(
         %HTTPoison.AsyncResponse{id: resp_id} = resp,
         fd,
         error_acc \\ %{}
       ) do
    receive do
      # TODO: parameterize the status value
      %HTTPoison.AsyncStatus{code: status_code, id: ^resp_id} ->
        HTTPoison.stream_next(resp)

        if status_code == 200 do
          poison_async_stream_to_file_handler(resp, fd)
        else
          poison_async_stream_to_file_handler(resp, fd, %{
            status_code: status_code
          })
        end

      %HTTPoison.AsyncHeaders{headers: headers, id: ^resp_id} ->
        HTTPoison.stream_next(resp)

        if Map.get(error_acc, :status_code, 200) == 200 do
          poison_async_stream_to_file_handler(resp, fd)
        else
          poison_async_stream_to_file_handler(resp, fd, Map.put(error_acc, :headers, headers))
        end

      %HTTPoison.AsyncChunk{chunk: chunk, id: ^resp_id} ->
        HTTPoison.stream_next(resp)

        if Map.get(error_acc, :status_code, 200) == 200 do
          IO.binwrite(fd, chunk)
          poison_async_stream_to_file_handler(resp, fd)
        else
          body = Map.get(error_acc, :body, "") <> chunk
          poison_async_stream_to_file_handler(resp, fd, Map.put(error_acc, :body, body))
        end

      %HTTPoison.AsyncEnd{id: ^resp_id} ->
        if Map.get(error_acc, :status_code, 200) == 200 do
          :ok
        else
          {:error, error_acc}
        end

      %HTTPoison.Error{id: ^resp_id} = error_resp ->
        {:error, error_resp}
    end
  end
end
