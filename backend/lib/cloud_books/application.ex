defmodule CloudBooks.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      CloudBooks.Repo,
      # Start the Telemetry supervisor
      CloudBooksWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: CloudBooks.PubSub},
      # Start the Endpoint (http/https)
      CloudBooksWeb.Endpoint,
      # Start a worker by calling: CloudBooks.Worker.start_link(arg)
      # {CloudBooks.Worker, arg}
      {CloudBooks.Worker.TokenStore, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CloudBooks.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CloudBooksWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
