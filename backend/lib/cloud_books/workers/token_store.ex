defmodule CloudBooks.StorageProviderToken do
  @type t :: %__MODULE__{
          access_token: String.t(),
          expires_at: DateTime.t(),
          storage_provider_id: integer()
        }
  defstruct [:access_token, :expires_at, :storage_provider_id]
end

defmodule CloudBooks.Worker.TokenStore do
  use GenServer
  alias CloudBooks.StorageProviderToken
  @table __MODULE__.Table

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(state) do
    :ets.new(@table, [:set, :named_table, :protected])
    {:ok, state}
  end

  @impl true
  def handle_call(
        {:insert_access_token, user_storage_provider_id, access_token, expires_at,
         storage_provider_id},
        _from,
        state
      ) do
    storage_provider_token = %StorageProviderToken{
      access_token: access_token,
      expires_at: expires_at,
      storage_provider_id: storage_provider_id
    }

    :ets.insert(
      @table,
      {user_storage_provider_id, storage_provider_token}
    )

    {:reply, {:ok, storage_provider_token}, state}
  end

  @impl true
  def handle_call({:get_access_token, user_storage_provider_id}, _from, state) do
    resp =
      :ets.lookup(@table, user_storage_provider_id)
      |> case do
        [{_, token_resp}] -> {:ok, token_resp}
        [] -> {:error, :not_found}
      end

    {:reply, resp, state}
  end

  @spec get_access_token(String.t()) :: {:ok, StorageProviderToken.t()} | {:error, :not_found}
  def get_access_token(user_storage_provider_id) do
    GenServer.call(
      __MODULE__,
      {:get_access_token, user_storage_provider_id}
    )
  end

  def insert_access_token(user_storage_provider_id, access_token, expires_at, storage_provider_id) do
    GenServer.call(
      __MODULE__,
      {:insert_access_token, user_storage_provider_id, access_token, expires_at,
       storage_provider_id}
    )
  end
end
