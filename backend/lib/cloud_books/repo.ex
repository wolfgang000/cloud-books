defmodule CloudBooks.Repo do
  use Ecto.Repo,
    otp_app: :cloud_books,
    adapter: Ecto.Adapters.Postgres

  use Paginator
end
