defmodule CloudBooks.Helper.StorageProviderApiHelperBehaviour do
  @moduledoc false

  @callback get_account_info(CloudBooks.StorageProviderToken.t()) ::
              {:error, Tesla.Env.t()} | {:ok, map()}
  @callback get_file(CloudBooks.StorageProviderToken.t(), String.t()) ::
              HTTPoison.AsyncResponse.t()
end

defmodule CloudBooks.Helper.StorageProviderApiHelper do
  @behaviour CloudBooks.Helper.StorageProviderApiHelperBehaviour
  # If this module gets too big then break apart by providers ex: StorageProviderApiGoogleHelper, StorageProviderApiDropboxHelper
  alias CloudBooks.StorageProviderToken
  alias CloudBooks.Accounts.StorageProvider

  @storage_provider_id_google_drive StorageProvider.storage_provider_id_google_drive()
  @storage_provider_id_dropbox StorageProvider.storage_provider_id_dropbox()

  def get_account_info(%StorageProviderToken{
        storage_provider_id: @storage_provider_id_google_drive,
        access_token: access_token
      }) do
    google_drive_conn = GoogleApi.Drive.V3.Connection.new(access_token)

    case GoogleApi.Drive.V3.Api.About.drive_about_get(google_drive_conn, fields: "user") do
      {:ok, %{user: %GoogleApi.Drive.V3.Model.User{} = user}} ->
        {:ok, %{id: user.emailAddress, name: user.displayName}}

      {:error, info} ->
        {:error, info}
    end
  end

  def get_account_info(%StorageProviderToken{
        storage_provider_id: @storage_provider_id_dropbox,
        access_token: access_token
      }) do
    HTTPoison.post(
      "https://api.dropboxapi.com/2/users/get_current_account",
      "null",
      [{"Authorization", "Bearer #{access_token}"}, {"Content-Type", "application/json"}]
    )
    |> case do
      {:ok, %HTTPoison.Response{status_code: 200, body: resp}} ->
        %{"account_id" => account_id, "name" => %{"display_name" => display_name}} =
          Poison.decode!(resp)

        {:ok, %{id: account_id, name: display_name}}

      error ->
        {:error, error}
    end
  end

  def get_file(
        %StorageProviderToken{
          storage_provider_id: @storage_provider_id_google_drive,
          access_token: access_token
        },
        file_id
      ) do
    url = "https://www.googleapis.com/drive/v2/files/#{file_id}?alt=media"

    HTTPoison.get!(url, [{"Authorization", "Bearer #{access_token}"}],
      stream_to: self(),
      async: :once,
      recv_timeout: 15000
    )
  end

  def get_file(
        %StorageProviderToken{
          storage_provider_id: @storage_provider_id_dropbox,
          access_token: access_token
        },
        file_id
      ) do
    url = "https://content.dropboxapi.com/2/files/download"

    headers = [
      {"Authorization", "Bearer #{access_token}"},
      {"Dropbox-API-Arg", "{\"path\": \"#{file_id}\"}"}
    ]

    HTTPoison.post!(url, "", headers,
      stream_to: self(),
      async: :once,
      recv_timeout: 15000
    )
  end

  def get_files(
        %StorageProviderToken{
          storage_provider_id: @storage_provider_id_google_drive,
          access_token: access_token
        },
        cursor
      ) do
    conn = GoogleApi.Drive.V3.Connection.new(access_token)

    with {:ok, resp} <- google_drive_get_files(conn, cursor) do
      files = Enum.map(resp.files, &%{id: &1.id, name: &1.name})

      {:ok, %{files: files, next_cursor: resp.nextPageToken}}
    end
  end

  # TODO: this implementation uses the search api which only handles a maximum of 10,000 matches
  # Find a better way to retrive all the files
  def get_files(
        %StorageProviderToken{
          storage_provider_id: @storage_provider_id_dropbox,
          access_token: access_token
        },
        cursor
      ) do
    with {:ok, resp} <- dropbox_get_files(access_token, cursor) do
      files =
        Enum.map(resp["matches"], fn %{"metadata" => %{"metadata" => metadata}} ->
          %{id: metadata["id"], name: metadata["name"]}
        end)

      {:ok, %{files: files, next_cursor: Map.get(resp, "cursor")}}
    end
  end

  defp google_drive_get_files(conn, ""),
    do:
      GoogleApi.Drive.V3.Api.Files.drive_files_list(conn,
        q: "mimeType='application/pdf' and trashed=false"
      )

  defp google_drive_get_files(conn, page_token),
    do:
      GoogleApi.Drive.V3.Api.Files.drive_files_list(conn,
        q: "mimeType='application/pdf' and trashed=false",
        pageToken: page_token
      )

  defp dropbox_get_files(access_token, "") do
    body = %{
      "query" => ".pdf",
      "options" => %{
        "file_status" => "active",
        "file_categories" => ["pdf"]
      }
    }

    HTTPoison.post(
      "https://api.dropboxapi.com/2/files/search_v2",
      Poison.encode!(body),
      [{"Authorization", "Bearer #{access_token}"}, {"Content-Type", "application/json"}]
    )
    |> case do
      {:ok, %HTTPoison.Response{status_code: 200, body: resp}} ->
        {:ok, Poison.decode!(resp)}

      error ->
        {:error, error}
    end
  end

  defp dropbox_get_files(access_token, cursor) do
    body = %{
      "cursor" => cursor
    }

    HTTPoison.post(
      "https://api.dropboxapi.com/2/files/search/continue_v2",
      Poison.encode!(body),
      [{"Authorization", "Bearer #{access_token}"}, {"Content-Type", "application/json"}]
    )
    |> case do
      {:ok, %HTTPoison.Response{status_code: 200, body: resp}} ->
        {:ok, Poison.decode!(resp)}

      error ->
        {:error, error}
    end
  end
end
