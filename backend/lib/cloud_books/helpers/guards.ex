defmodule CloudBooks.Helper.Guards do
  defguard is_undefined(expr) when is_nil(expr) or expr == ""
end
