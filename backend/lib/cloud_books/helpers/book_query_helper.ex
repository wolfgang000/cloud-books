defmodule CloudBooks.Helper.BookQueryHelper do
  import Ecto.Query, warn: false
  alias CloudBooks.Repo
  alias CloudBooks.Core.Book

  @default_page_size 25

  def get_user_books_query(user_id) do
    from(b in Book,
      as: :book,
      join: usp in assoc(b, :user_storage_provider),
      as: :user_storage_provider,
      where: usp.user_id == ^user_id,
      order_by: [desc: b.updated_at, desc: b.id]
    )
  end

  def paginate_by_updated_at_and_id(query) do
    Repo.paginate(query,
      cursor_fields: [{:updated_at, :desc}, {:id, :desc}],
      limit: @default_page_size
    )
  end

  def paginate_by_updated_at_and_id_with_page_cursor(query, next_page_cursor) do
    Repo.paginate(query,
      after: next_page_cursor,
      cursor_fields: [{:updated_at, :desc}, {:id, :desc}],
      limit: @default_page_size
    )
  end

  def filter_by_reading_status_id(query, %{"reading_status_id" => reading_status_id})
      when is_integer(reading_status_id) or is_binary(reading_status_id),
      do: from([book: b] in query, where: b.reading_status_id == ^reading_status_id)

  def filter_by_reading_status_id(query, %{"reading_status_id" => [_ | _] = reading_status_ids}),
    do: from([book: b] in query, where: b.reading_status_id in ^reading_status_ids)

  def filter_by_reading_status_id(query, _), do: query
end
