defmodule CloudBooks.Helper.S3Helper do
  alias ExAws.S3

  @spec s3_bucket() :: String.t()
  def s3_bucket() do
    Application.get_env(:ex_aws, :s3)[:bucket]
  end

  @spec upload_file(String.t(), String.t(), any) :: {:ok, any} | {:error, any}
  def upload_file(file_path, object_key, opts \\ []) do
    file_path
    |> S3.Upload.stream_file()
    |> S3.upload(s3_bucket(), object_key, opts)
    |> ExAws.request()
  end

  def delete_all_objects(prefix) do
    stream =
      ExAws.S3.list_objects(s3_bucket(), prefix: prefix)
      |> ExAws.stream!()
      |> Stream.map(& &1.key)

    ExAws.S3.delete_all_objects(s3_bucket(), stream)
    |> ExAws.request()
  end

  def get_presigned_url(http_method, object_key) do
    config = ExAws.Config.new(:s3, [])
    S3.presigned_url(config, http_method, s3_bucket(), object_key)
  end
end
