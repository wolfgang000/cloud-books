defmodule CloudBooks.Helper.SyncManager do
  # TODO: this module is tightly coupled with the google drive api
  # investigate how dropbox and onedrive handle the search and download of their files and refator accordingly
  # Also add tests

  require Logger
  alias CloudBooks.Helper.{TokenManager, PDFUtil, S3Helper}
  alias CloudBooks.{Accounts, Core, Util}
  alias CloudBooksWeb.StorageView

  @temp_dir "/tmp"

  def storage_provider_api_helper,
    do: Application.fetch_env!(:cloud_books, :storage_provider_api_helper)

  def sync(user_storage_provider_id) do
    # TODO: Investigate how to implement a thread pool to handle multiple downloads in parallel
    Task.start(fn ->
      try do
        with {:ok, user} <- Accounts.get_user_by_storage_provider(user_storage_provider_id),
             {:set_sync_flag_to_started, {1, _}} <-
               {:set_sync_flag_to_started,
                Accounts.update_users_storage_provider_sync_flag_to_started(
                  user_storage_provider_id
                )},
             {:ok, sync_started_user_storage_provider} <-
               Accounts.get_users_storage_provider(user_storage_provider_id) do
          CloudBooksWeb.Endpoint.broadcast(
            "user:#{user.id}",
            "sync:started",
            StorageView.render("storage.json", %{storage: sync_started_user_storage_provider})
          )

          Stream.unfold("", fn
            nil ->
              nil

            cursor ->
              with {:ok, token} <- TokenManager.get_access_token(user_storage_provider_id),
                   {:ok, files_resp} <- storage_provider_api_helper().get_files(token, cursor) do
                file_ids =
                  Core.get_file_ids_not_present_in_users_books(
                    Enum.map(files_resp.files, & &1.id),
                    String.to_integer(user_storage_provider_id)
                  )

                for f <- Enum.filter(files_resp.files, &(&1.id in file_ids)) do
                  create_book(user_storage_provider_id, f)
                  |> case do
                    {:error, error} ->
                      Logger.error(
                        "Unexpected error on new book creation while fetching files, user_storage_provider: #{
                          user_storage_provider_id
                        }, #{inspect(f)} : #{inspect(error)}"
                      )

                    r ->
                      r
                  end
                end

                {cursor, files_resp.next_cursor}
              else
                error ->
                  Logger.error(
                    "Unexpected error while fetching files, user_storage_provider: #{
                      user_storage_provider_id
                    } : #{inspect(error)}"
                  )

                  {cursor, nil}
              end
          end)
          |> Stream.run()

          # Todo: refactor this
          with {:set_sync_flag_to_ended, {1, _}} <-
                 {:set_sync_flag_to_ended,
                  Accounts.update_users_storage_provider_sync_flag_to_ended(
                    user_storage_provider_id
                  )},
               {:ok, sync_ended_user_storage_provider} <-
                 Accounts.get_users_storage_provider(user_storage_provider_id) do
            CloudBooksWeb.Endpoint.broadcast(
              "user:#{user.id}",
              "sync:ended",
              StorageView.render("storage.json", %{storage: sync_ended_user_storage_provider})
            )
          end
        else
          {:set_sync_flag_to_started, _} -> nil
          _e -> nil
        end
      rescue
        error ->
          Logger.error(
            "Unexpected error while syncing, releasing the lock, user_storage_provider: #{
              user_storage_provider_id
            } : #{inspect(error)}"
          )

          resp =
            Accounts.update_users_storage_provider_sync_flag_to_ended(user_storage_provider_id)

          Logger.error(
            "Lock release response user_storage_provider: #{user_storage_provider_id} : #{
              inspect(resp)
            }"
          )
      end
    end)
  end

  def create_book(user_storage_provider_id, object_file) do
    with {:ok, storage_provider_token} <-
           TokenManager.get_access_token(user_storage_provider_id),
         {:ok, temp_file_path} <- download_file(storage_provider_token, object_file.id),
         {:ok, metadata} <- PDFUtil.get_pdf_metadata(temp_file_path),
         {:ok, thumbnail_s3_object_key} <-
           create_and_upload_thumbnail(
             temp_file_path,
             "#{user_storage_provider_id}/#{UUID.uuid1()}.jpg"
           ) do
      # Todo: handle file deletion on error
      File.rm(temp_file_path)

      Core.create_book(%{
        title: Map.get(metadata, :title) || object_file.name,
        author: Map.get(metadata, :author),
        num_pages: Map.get(metadata, :num_pages) || 1,
        current_page: 1,
        file_name: object_file.name,
        file_id: object_file.id,
        thumbnail_s3_object_key: thumbnail_s3_object_key,
        user_storage_provider_id: user_storage_provider_id
      })
    else
      {:error, e} -> {:error, e}
      e -> {:error, e}
    end
  end

  def download_file(storage_provider_token, file_id) do
    temp_file_path = "#{@temp_dir}/#{DateTime.to_string(DateTime.utc_now())}-#{UUID.uuid1()}"

    with {:ok, file_pid} <- File.open(temp_file_path, [:write, :binary]) do
      async_request = storage_provider_api_helper().get_file(storage_provider_token, file_id)

      case Util.poison_async_stream_to_file(async_request, file_pid) do
        :ok ->
          File.close(file_pid)
          {:ok, temp_file_path}

        error ->
          {:error,
           "Unable to download file #{file_id} with token #{inspect(storage_provider_token)}: #{
             inspect(error)
           }"}
      end
    else
      error -> {:error, "Unable to open/create file #{temp_file_path} : #{inspect(error)}"}
    end
  end

  def create_and_upload_thumbnail(pdf_file_path, s3_object_key) do
    temp_image_path = "#{pdf_file_path}.jpg"

    with {:pdf_to_cairo, :ok} <-
           {:pdf_to_cairo,
            PDFUtil.pdf_to_cairo([
              "-jpeg",
              "-q",
              "-singlefile",
              "-scale-to",
              "540",
              pdf_file_path,
              pdf_file_path
            ])},
         {:upload_file, {:ok, _}} <-
           {:upload_file,
            S3Helper.upload_file(temp_image_path, s3_object_key,
              content_type: "image/jpeg",
              timeout: 60_000
            )} do
      File.rm(temp_image_path)
      {:ok, s3_object_key}
    else
      {:pdf_to_cairo, error} ->
        {:error, "Unable to create thumbnail image #{pdf_file_path} : #{inspect(error)}"}

      {:upload_file, error} ->
        File.rm(temp_image_path)
        {:error, "Unable to upload thumbnail image #{temp_image_path} : #{inspect(error)}"}
    end
  end
end
