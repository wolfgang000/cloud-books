defmodule CloudBooks.Helper.PDFUtil do
  import CloudBooks.Helper.Guards

  def pdf_to_cairo(args) do
    case System.cmd("pdftocairo", args, stderr_to_stdout: true) do
      {_, 0} ->
        :ok

      error ->
        {:error,
         "Unable to generate image from file with args #{inspect(args)}, #{inspect(error)}"}
    end
  end

  @spec get_pdf_metadata(String.t()) :: {:ok, map} | {:error, String.t()}
  def get_pdf_metadata(input_pdf_path) do
    case System.cmd("pdfinfo", [input_pdf_path], stderr_to_stdout: true) do
      {data, 0} ->
        data
        |> String.split("\n")
        |> Enum.map(fn item ->
          item
          |> String.split(":")
          |> Enum.map(&String.trim/1)
        end)
        |> Enum.reduce([], fn item, acc ->
          case get_item_metadata(item) do
            nil -> acc
            value -> [value | acc]
          end
        end)
        |> Map.new()
        |> case do
          metadata ->
            {:ok, metadata}
        end

      _ ->
        {:error, "Unable to read pdf metadata from file #{input_pdf_path}"}
    end
  end

  defp get_item_metadata(["Title", value]) when not is_undefined(value), do: {:title, value}
  defp get_item_metadata(["Subject", value]) when not is_undefined(value), do: {:subject, value}
  defp get_item_metadata(["Keywords", value]) when not is_undefined(value), do: {:keywords, value}
  defp get_item_metadata(["Author", value]) when not is_undefined(value), do: {:author, value}
  defp get_item_metadata(["Pages", value]), do: {:num_pages, Integer.parse(value) |> elem(0)}
  defp get_item_metadata(["Encrypted", "no"]), do: {:is_encrypted, false}
  defp get_item_metadata(["Encrypted", "yes"]), do: {:is_encrypted, true}
  defp get_item_metadata(_), do: nil
end
