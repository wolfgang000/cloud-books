defmodule CloudBooks.Helper.TokenManager do
  alias CloudBooks.Worker.TokenStore
  alias CloudBooks.Encryption.AES
  alias CloudBooks.Accounts
  alias CloudBooks.Accounts.{UserStorageProvider, StorageProvider}
  alias CloudBooks.StorageProviderToken

  @storage_provider_id_google_drive StorageProvider.storage_provider_id_google_drive()
  @storage_provider_id_dropbox StorageProvider.storage_provider_id_dropbox()
  @expiration_buffer_time 300

  def google_oauth, do: Application.fetch_env!(:cloud_books, :google_oauth)
  def dropbox_oauth, do: Application.fetch_env!(:cloud_books, :dropbox_oauth)

  @spec get_access_token(String.t()) ::
          {:error, String.t()} | {:ok, StorageProviderToken.t()}
  def get_access_token(user_storage_provider_id) do
    # There is a race condition here, if TokenManager.get_access_token is called from 2 processes at the same time and if the token is expired or
    # non-existent in the table then TokenManager.update_access_token will be called 2 times and, therefore one call will overwrite the value of
    # the access_token from the previous call, however this should not cause any problems since both tokens should be valid
    case TokenStore.get_access_token(user_storage_provider_id) do
      {:error, :not_found} ->
        update_access_token(user_storage_provider_id)

      {:ok, %{expires_at: expires_at} = storage_provider_token} ->
        if DateTime.diff(expires_at, DateTime.utc_now()) - @expiration_buffer_time > 0 do
          {:ok, storage_provider_token}
        else
          update_access_token(user_storage_provider_id)
        end
    end
  end

  defp update_access_token(user_storage_provider_id) do
    with {:ok, user_storage_provider} <-
           Accounts.get_users_storage_provider(user_storage_provider_id),
         {:ok, %OAuth2.Client{token: token}} <-
           refresh_token_user_storage_provider(user_storage_provider),
         {:ok, %StorageProviderToken{} = storage_provider_token} <-
           TokenStore.insert_access_token(
             user_storage_provider_id,
             token.access_token,
             DateTime.from_unix!(token.expires_at),
             user_storage_provider.storage_provider_id
           ) do
      {:ok, storage_provider_token}
    else
      {:error, :not_found} ->
        {:error, "The user_storage_provider_id: #{user_storage_provider_id} seems to be invalid"}

      # TODO: handle refresh_token error response
      e ->
        {:error,
         "Unknow error on update_access_token, user_storage_provider_id: #{
           user_storage_provider_id
         }, #{inspect(e, pretty: true)}"}
    end
  end

  defp refresh_token_user_storage_provider(%UserStorageProvider{
         storage_provider_id: @storage_provider_id_google_drive,
         refresh_token_encrypted: refresh_token_encrypted
       }),
       do: google_oauth().get_refresh_token(AES.decrypt(refresh_token_encrypted))

  defp refresh_token_user_storage_provider(%UserStorageProvider{
         storage_provider_id: @storage_provider_id_dropbox,
         refresh_token_encrypted: refresh_token_encrypted
       }) do
    dropbox_oauth().get_refresh_token(AES.decrypt(refresh_token_encrypted))
    |> case do
      {:ok, client = %OAuth2.Client{token: token}} ->
        %{"access_token" => access_token, "expires_in" => expires_in} =
          Poison.decode!(token.access_token)

        updated_token =
          token
          |> Map.put(
            :expires_at,
            DateTime.utc_now()
            |> DateTime.add(expires_in, :second)
            |> DateTime.to_unix()
          )
          |> Map.put(
            :access_token,
            access_token
          )

        {:ok, %{client | token: updated_token}}

      err ->
        err
    end
  end
end
