defmodule CloudBooks.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias CloudBooks.{Repo, Util}

  alias CloudBooks.Accounts.{
    User,
    UserStorageProvider,
    Session
  }

  @session_expiration_time_in_seconds Session.default_expiration_time_in_seconds()

  def create_user_storage_provider(attrs) do
    %UserStorageProvider{}
    |> UserStorageProvider.creation_changeset(attrs)
    |> Repo.insert()
  end

  def list_users_storage_providers(user_id) do
    from(usp in UserStorageProvider,
      join: user in assoc(usp, :user),
      where: usp.user_id == ^user_id
    )
    |> Repo.all()
  end

  def get_users_storage_provider(id, user_id) do
    from(usp in UserStorageProvider,
      join: user in assoc(usp, :user),
      where: usp.user_id == ^user_id,
      where: usp.id == ^id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def update_users_storage_provider_sync_flag_to_started(id) do
    from(usp in UserStorageProvider,
      where: usp.id == ^id and usp.is_synchronizing == false
    )
    |> Repo.update_all(set: [is_synchronizing: true, last_started_at: DateTime.utc_now()])
  end

  def update_users_storage_provider_sync_flag_to_ended(id) do
    from(usp in UserStorageProvider,
      where: usp.id == ^id and usp.is_synchronizing == true
    )
    |> Repo.update_all(set: [is_synchronizing: false, last_ended_at: DateTime.utc_now()])
  end

  def delete_users_storage_provider(%UserStorageProvider{} = users_storage_provider) do
    Repo.delete(users_storage_provider)
  end

  def get_users_storage_provider(id),
    do: Repo.get(UserStorageProvider, id) |> Util.return_value_or_not_found()

  @spec get_users_storage_provider_by_clauses(Keyword.t() | map()) ::
          {:error, :not_found} | {:ok, any}
  def get_users_storage_provider_by_clauses(clauses) do
    Repo.get_by(UserStorageProvider, clauses)
    |> Util.return_value_or_not_found()
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def get_user!(id), do: Repo.get!(User, id)

  def get_user_by_storage_provider(user_storage_provider_id) do
    from(u in User,
      join: usp in assoc(u, :user_storage_providers),
      where: usp.id == ^user_storage_provider_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def register_user(attrs) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def authenticate_user(email, plain_text_password) do
    query = from u in User, where: u.email == ^email

    Repo.one(query)
    |> check_password(plain_text_password)
  end

  defp check_password(nil, _) do
    {:error,
     Ecto.Changeset.change({%{}, %{email: :string}}, %{email: ""})
     |> Ecto.Changeset.add_error(:email, "wrong email or password")}
  end

  defp check_password(user, plain_text_password) do
    if Argon2.verify_pass(plain_text_password, user.password_hash) do
      {:ok, user}
    else
      {:error,
       Ecto.Changeset.change({%{}, %{email: :string}}, %{email: user.email})
       |> Ecto.Changeset.add_error(:email, "wrong email or password")}
    end
  end

  def update_user_last_login_date(user) do
    update_user(user, %{last_login: DateTime.utc_now()})
  end

  def create_session(attrs \\ %{}) do
    attrs =
      attrs
      |> Map.put(:session_key, :crypto.strong_rand_bytes(32) |> Base.encode64(padding: false))
      |> Map.put(
        :expiration_date,
        DateTime.add(DateTime.utc_now(), @session_expiration_time_in_seconds, :second)
      )
      |> Map.put(:inserted_at, DateTime.utc_now())

    %Session{}
    |> Session.changeset(attrs)
    |> Repo.insert()
  end

  def get_session_by_id_with_preloaded_data(session_id) do
    from(session in Session,
      where: session.id == ^session_id,
      join: user in assoc(session, :user),
      preload: [user: user]
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_session_by_key_with_preloaded_data(session_key) do
    from(session in Session,
      where: session.session_key == ^session_key,
      join: user in assoc(session, :user),
      preload: [user: user]
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def delete_session_by_id(session_id) do
    Repo.get!(Session, session_id)
    |> Repo.delete()
  end
end
