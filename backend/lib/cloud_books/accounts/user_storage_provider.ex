defmodule CloudBooks.Accounts.UserStorageProvider do
  use Ecto.Schema
  import Ecto.Changeset
  alias CloudBooks.Encryption.AES

  schema "user_storage_provider" do
    field :refresh_token, :string, virtual: true
    field :refresh_token_encrypted, :binary
    field :account_id, :string
    field :account_name, :string
    field :is_synchronizing, :boolean
    field :last_started_at, :utc_datetime
    field :last_ended_at, :utc_datetime
    belongs_to :storage_provider, CloudBooks.Accounts.StorageProvider
    belongs_to :user, CloudBooks.Accounts.User

    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :storage_provider_id,
    :user_id,
    :account_id,
    :account_name
  ]

  @attrs @required_attrs ++ [:is_synchronizing, :last_started_at, :last_ended_at]
  @doc false
  def creation_changeset(user_storage_provider, attrs) do
    user_storage_provider
    |> cast(attrs, @attrs ++ [:refresh_token])
    |> validate_required(@required_attrs ++ [:refresh_token])
    |> foreign_key_constraint(:storage_provider_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint([:account_id, :storage_provider_id],
      name: :user_storage_provider_account_id_storage_provider_id_index
    )
    |> put_refresh_token_encrypted()
  end

  defp put_refresh_token_encrypted(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{refresh_token: refresh_token_plain}} ->
        put_change(changeset, :refresh_token_encrypted, AES.encrypt(refresh_token_plain))

      _ ->
        changeset
    end
  end
end
