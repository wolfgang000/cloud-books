defmodule CloudBooks.Accounts.StorageProvider do
  use Ecto.Schema

  schema "storage_providers" do
    field :name, :string
  end

  @storage_provider_id_google_drive 1
  def storage_provider_id_google_drive, do: @storage_provider_id_google_drive

  @storage_provider_id_dropbox 2
  def storage_provider_id_dropbox, do: @storage_provider_id_dropbox

  @storage_provider_ids %{
    google: @storage_provider_id_google_drive,
    dropbox: @storage_provider_id_dropbox
  }
  def storage_provider_ids, do: @storage_provider_ids
end
