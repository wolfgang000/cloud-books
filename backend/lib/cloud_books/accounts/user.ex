defmodule CloudBooks.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias CloudBooks.Accounts.UserStorageProvider

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string, virtual: true
    field :password_hash, :binary
    field :last_login, :utc_datetime
    has_many :user_storage_providers, UserStorageProvider

    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :email,
    :name
  ]

  @attrs @required_attrs ++ [:last_login]

  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> downcase_email()
  end

  @doc false
  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :name])
    |> validate_required([:email, :password, :name])
    |> validate_length(:password, min: 7)
    |> validate_format(:email, ~r/@/)
    |> downcase_email()
    |> unique_constraint(:email)
    |> put_password_hash()
  end

  defp downcase_email(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{email: email}} ->
        put_change(
          changeset,
          :email,
          email
          |> String.downcase()
          |> String.trim()
        )

      _ ->
        changeset
    end
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Argon2.hash_pwd_salt(pass))

      _ ->
        changeset
    end
  end
end
