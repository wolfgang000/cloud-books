defmodule CloudBooks.Accounts.Session do
  use Ecto.Schema
  import Ecto.Changeset

  # TODO: maybe hash session_key
  schema "sessions" do
    field :session_key, :string
    field :expiration_date, :utc_datetime
    field :inserted_at, :utc_datetime
    belongs_to(:user, CloudBooks.Accounts.User)
  end

  # 60 Days
  @default_expiration_time_in_seconds 60 * 60 * 24 * 60
  def default_expiration_time_in_seconds, do: @default_expiration_time_in_seconds

  @required_attrs [
    :user_id,
    :session_key,
    :expiration_date,
    :inserted_at
  ]

  @attrs @required_attrs

  @doc false
  def changeset(sessions, attrs) do
    sessions
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:session_key)
  end
end
