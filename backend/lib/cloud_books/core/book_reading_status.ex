defmodule CloudBooks.Core.ReadingStatus do
  use Ecto.Schema

  schema "reading_status" do
    field :name, :string
  end

  @id_unknown 1
  def id_unknown, do: @id_unknown

  @id_reading 2
  def id_reading, do: @id_reading

  @id_plan_to_read 3
  def id_plan_to_read, do: @id_plan_to_read

  @id_completed 4
  def id_completed, do: @id_completed

  @ids %{
    unknown: @id_unknown,
    reading: @id_reading,
    plan_to_read: @id_plan_to_read,
    completed: @id_completed
  }
  def ids, do: @ids
end
