defmodule CloudBooks.Core.Book do
  use Ecto.Schema
  import Ecto.Changeset

  # TODO: add checksum field to identify repeated files
  # Replace author field with a foreign key to a new autor table
  schema "books" do
    field :title, :string
    field :author, :string
    field :num_pages, :integer
    field :current_page, :integer
    field :file_id, :string
    field :file_name, :string
    field :thumbnail_s3_object_key, :string
    belongs_to :user_storage_provider, CloudBooks.Accounts.UserStorageProvider
    belongs_to :reading_status, CloudBooks.Core.ReadingStatus

    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :title,
    :file_id,
    :file_name,
    :num_pages,
    :current_page,
    :user_storage_provider_id
  ]

  @attrs @required_attrs ++ [:author, :thumbnail_s3_object_key, :reading_status_id]

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:user_storage_provider_id)
    |> foreign_key_constraint(:reading_status_id)
    |> unique_constraint([:file_id, :user_storage_provider_id])
  end

  def changeset_update(book, attrs) do
    book
    |> cast(attrs, [:current_page, :reading_status_id])
    |> foreign_key_constraint(:reading_status_id)
  end
end
